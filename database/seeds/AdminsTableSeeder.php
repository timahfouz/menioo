<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('admins')->insert([
            'name' => 'Super Admin',
            'email' => 'admin@menioo.com',
            'password' => bcrypt('admin'),
        ]);
    }
}
