<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('parent_id')->default(0);
            $table->string('name', 250);
            $table->text('description');
            $table->enum('display_as', ['grid', 'list']);
            $table->tinyInteger('num_of_rows')->nullable();
            $table->enum('grid_title_position', ['top', 'middle', 'bottom'])->nullable();
            $table->tinyInteger('display_similar_items')->default(0);
            $table->tinyInteger('mark_as_new')->default(0);
            $table->tinyInteger('mark_as_signature')->default(0);
            $table->text('additional_notes')->nullable();
            $table->integer('media_id')->nullable()->unsigned();
            $table->integer('video_id')->nullable()->unsigned();
            $table->tinyInteger('is_active')->default(0);
            $table->integer('section_id')->nullable()->unsigned()->after('parent_id');
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('menu_sections')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('media_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('video_id')->references('id')->on('media')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
