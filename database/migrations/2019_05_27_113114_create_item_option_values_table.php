<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemOptionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_option_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_option_id');
            $table->string('value');
            $table->float('price')->nullable();
            $table->timestamps();
            $table->foreign('item_option_id')->references('id')->on('item_options')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_option_values');
    }
}
