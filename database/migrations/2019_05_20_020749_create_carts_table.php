<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('customer_user_id')->nullable();
            $table->bigInteger('table_number')->nullable();
            $table->float('total_cost')->default(0);
            $table->boolean('ordered')->default(0);
            $table->timestamps();
            $table->foreign('customer_user_id')->references('id')->on('customer_users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('customer_users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
