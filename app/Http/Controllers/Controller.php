<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function jsonResponse($code, $message, $data=[], $jcode=200)
    {
        // if (is_array($data) && !count($data)) {
        //     $data = (object)array();
        // }
        return response()->json([
            'code' => $code,
            'message' => $message,
            'data' => $data
        ], $jcode);
    }
}
