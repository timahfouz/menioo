<?php

namespace App\Http\Controllers;

use App\RecommendedItem;
use Illuminate\Http\Request;

class RecommendedItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecommendedItem  $recommendedItem
     * @return \Illuminate\Http\Response
     */
    public function show(RecommendedItem $recommendedItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecommendedItem  $recommendedItem
     * @return \Illuminate\Http\Response
     */
    public function edit(RecommendedItem $recommendedItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecommendedItem  $recommendedItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecommendedItem $recommendedItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecommendedItem  $recommendedItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecommendedItem $recommendedItem)
    {
        //
    }
}
