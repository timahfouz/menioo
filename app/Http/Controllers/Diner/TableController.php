<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\TableService;
use App\Http\Services\Admin\CustomerServices;

Use URL;

class TableController extends DinerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $deviceObj;
    private $customerObj;

    public function __construct(TableService $tableObj, CustomerServices $customerObj){
        parent::__construct();

        $this->tableObj = $tableObj;
        $this->customerObj = $customerObj;
    }

    public function index(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['title'] = (object)[
            'name' => 'Tables',
            'child' => (object)[
                'name' => 'Create Table',
                'url' => $customerID ? URL::to('diner/tables/create?venue_id='.$customerID) : URL::to('diner/tables/create')
            ]
        ];
        $this->data['tables'] = $this->tableObj->getAll(['customer_id' => $customerID],[],10);

        return view('diner.tables.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
     {
         $customerID = (int)$request->venue_id ? (int)$request->venue_id : 0;

         $customer = (int)$request->venue_id ? $this->customerObj->find(['id'=>(int)$request->venue_id]): $this->data['user_branch'];
         $this->data['items'] = $customer ? $customer->items() : [];

         $this->data['title'] = (object)[
             'name' => 'Create New Table',
             'url' => $customerID ? URL::to('diner/tables?venue_id='.$customerID) : URL::to('diner/tables'),
             'parent' => (object)['name' => 'Tables']
         ];
         //$this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

         $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;
         $this->data['customer_id'] = (int)$request->venue_id ? (int)$request->venue_id : $this->data['user_branch']->id;
         return view('diner.tables.create', $this->data);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
    */
     public function store(Request $request)
     {
         if(!$request->filled('number') || !is_numeric($request->number)) {
           return redirect()->back()->with('fail', 'Enter a valid table number!');
         }
         $data = $request->except(['_token','image','video']);
         //return $data;

         $table = $this->tableObj->insert($data);
         if ($table) {
             return $this->data['user_branch']->id != $table->customer_id ?
                 redirect()->route('diner.tables.index', ['venue_id' => $table->customer_id])->with(['success' => 'added successfully']) :
                 redirect()->route('diner.tables.index')->with(['success' => 'added successfully']);
         } else {
             return redirect()->back()->with('fail', 'Whoops something wrong');
         }

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $customerID = (int)request()->venue_id ? (int)request()->venue_id : 0;

            //$customer = $this->data['user_branch'];
        $customer = (int)request()->venue_id ? $this->customerObj->find(['id'=>(int)request()->venue_id]): $this->data['user_branch'];
        $this->data['customer_id'] = $customer->id;
        $this->data['items'] = $customer ? $customer->items() : [];

        $table = $this->tableObj->find(['id' => $id]);
        $this->data['table'] = $table;

        if ($this->data['table']) {
            $this->data['title'] = (object)[
                'name' => 'Edit '.$table->title,
                'url' => $customerID ? URL::to('diner/tables?venue_id=').$customerID : URL::to('diner/tables').$customerID,
                'parent' => (object)['name' => 'Table'
            ]];

            return view('diner.tables.edit', $this->data);
        } else {
            return view('diner.404');
        }
    }

    public function update(Request $request, $id)
    {
        if(!$request->filled('number') || !is_numeric($request->number)) {
          return redirect()->back()->with('fail', 'Enter a valid table number!');
        }
        $data = $request->except(['_token','_method']);

        $table = $this->tableObj->find(['id'=>$id]);
        if ($table) {
            $this->tableObj->update(['id'=>$id],$data);
            return $this->data['user_branch']->id != $table->customer_id ?
                redirect()->route('diner.tables.index', ['venue_id' => $table->customer_id])->with(['success' => 'added successfully']) :
                redirect()->route('diner.tables.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->tableObj->delete(['id' => $id]);
        return redirect()->back()->with(['success' => 'Devices deleted successfully.']);
    }

    public function deleteall($ids){
        $ids = array_map ('intval', explode(",",$ids));
        foreach ($ids as $id) {
            $this->destroy($id);
        }
        return ['success' => 'Devices deleted successfully.'];
    }
}
