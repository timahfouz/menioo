<?php

namespace App\Http\Controllers\Diner;

use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\MenuSectionsService;
use App\Http\Services\Common\Helper;

use App\Http\Services\Diner\ItemPriceService;
use App\Http\Services\Diner\ItemOptionService;
use App\Http\Services\Diner\ItemOptionValueService;
use App\Http\Services\Diner\ItemSideDishService;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Diner\ItemIngredientWarningService;

use Illuminate\Http\Request;
Use URL;

class VenueController extends DinerController
{
    private $customerObj;
    private $menuObj;
    private $menuSectionObj;
    private $itemObj;
    private $helperObj;
    private $itemPriceObj;
    private $itemOptionObj;
    private $itemOptionValueObj;
    private $itemSideDishObj;
    private $itemIngredientObj;
    private $arrangmentObj;


    public function __construct(
        CustomerServices $customerObj,
        MenuSectionsService $menuSectionObj,
        ItemService $itemObj,
        MenuService $menuObj,
        ItemPriceService $itemPriceObj,
        ItemOptionService $itemOptionObj,
        ItemOptionValueService $itemOptionValueObj,
        ItemSideDishService $itemSideDishObj,
        ItemIngredientWarningService $itemIngredientObj,
        ArrangmentService $arrangmentObj,
        Helper $helperObj
    )
    {
        parent::__construct();

        $this->customerObj = $customerObj;
        $this->menuObj = $menuObj;
        $this->menuSectionObj = $menuSectionObj;
        $this->itemObj = $itemObj;
        $this->itemPriceObj = $itemPriceObj;
        $this->itemOptionObj = $itemOptionObj;
        $this->itemOptionValueObj = $itemOptionValueObj;
        $this->itemSideDishObj = $itemSideDishObj;
        $this->itemIngredientObj = $itemIngredientObj;
        $this->arrangmentObj = $arrangmentObj;

        $this->helperObj = $helperObj;
    }

    public function index(Request $request)
    {
        $conditions = [['id', '>', 0]];

        if ($request->keyword) {
            $conditions[] = ['name', 'like', '%' . $request->keyword . '%'];
        }

        if ($request->is_active != '') {
            $conditions['is_active'] = $request->is_active;
        }

        if ($request->country_id) {
            $conditions['country_id'] = $request->country_id;
        }

        if ($request->city_id) {
            $conditions['city_id'] = $request->city_id;
        }

        $per_page = $request->per_page ? $request->per_page : 20;

        $this->data['customers'] = $this->customerObj->getAll($conditions, $per_page);
        $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);
        $total_count = $this->customerObj->getTotalCount();

        $this->data['title'] = (object)['name' => 'Customers', 'url' => URL::to('admin/customer'),
            'additional' => (object)['name' => 'Export', 'url' => 'javascript:void(0)', 'class' => 'plain-btn'],
            'child' => (object)['name' => 'Create Customer', 'url' => URL::to('admin/customer/create'), 'subtitle' => $total_count . ' Customers']
        ];

        return view('diner.venue.index', $this->data);

    }

    public function create(Request $request)
    {
        $this->data['title'] = (object)['name' => 'Create New Venue', 'url' => URL::to('diner/menu'),
            'parent' => (object)['name' => 'Menus']];
        //$this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

        $this->data['venue_id'] = (int)$request->venue_id ? (int)$request->venue_id : 0;
        return view('diner.venue.create', $this->data);
    }

    public function store(Request $request)
    {
        $mainBranch = $this->data['current_user']->mainBranch();

        $data['parent_id'] = $mainBranch->id;
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['company_name'] = $mainBranch->company_name;
        $data['occupation'] = $mainBranch->occupation;
        $data['email'] = $mainBranch->email;
        $data['mobile_number'] = $mainBranch->mobile_number;
        $data['country_id'] = $mainBranch->country_id;
        $data['state'] = $mainBranch->state;

        $data['address'] = $request->address;
        $data['timezone'] = $request->timezone;
        $data['website'] = $request->website;
        $data['instagram'] = $request->instagram;
        $data['facebook'] = $request->facebook;
        $data['twitter'] = $request->twitter;
        $data['foursquare'] = $request->foursquare;

        $venue = $this->customerObj->insert($data);
        if ($venue) {
            if ($request->hasFile('logo')) {
              $media = $this->helperObj->upload($request->file('logo'),['title' => $data['name'], 'customer_id' => $venue->id]);
              $this->customerObj->update(['id'=>$venue->id],['logo_id'=>$media->id]);
            }

            return redirect()->route('diner.venue.edit', ['id' => $venue->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function show($id)
    {
        $this->data['current_venue'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_venue']) {
            $this->data['title'] = (object)['name' => 'Customer Details', 'url' => URL::to('admin/customer'),
                'additional' => (object)['name' => 'Login AS ' . $this->data['current_venue']->name, 'url' => URL::to('admin/login/customer/' . $this->data['current_venue']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer/' . $this->data['current_venue']->id . '/edit'), 'subtitle' => $this->data['current_venue']->name]
            ];
            return view('diner.venue.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    public function edit($id)
    {
        $currentVenue = $this->customerObj->find(['id' => $id]);
        $this->data['current_venue'] = $currentVenue;

        if ($this->data['current_venue']) {
            $this->data['title'] = (object)['name' => 'Edit '.$currentVenue->name, 'url' => URL::to('admin/customer'),
                'parent' => (object)['name' => 'Venue']];

            return view('diner.venue.edit', $this->data);
        } else {
            return view('admin.404');
        }
    }

    public function update(Request $request, $id)
    {
        if(!$request->name)
            return redirect()->back()->with(['fail' => 'name required!']);

        $current_venu = $this->customerObj->find(['id' => $id]);
        if ($current_venu) {
            $data = $request->only(['name', 'description','address','timezone','website','instagram','facebook','twitter','foursquare','state']);

            if ($request->hasfile('logo')) {
              $media = $this->helperObj->upload($request->file('logo'), ['title' => $data['name'], 'customer_id' => $id],'edit', $current_venu->logo);
              $data['logo_id'] = $media->id;
            }

            $current_venu = $this->customerObj->update(['id' => $id], $data);
            if ($current_venu) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    public function import(Request $request)
    {
        $venueID = $request->venue_id ? (int)$request->venue_id : $this->data['user_branch']->id;
        $venue = $this->customerObj->find(['id' => $venueID]);
        if(!$venue)
          return redirect()->back()->with(['fail'=>'Venue not existed!']);
        if(! isset($request->type) ){
            $menuID = $request->menu_id;
            $menu = $this->menuObj->find(['id'=>$menuID]);

            if($menu) {
                $sections = $menu->sections;
                $oldSubmenus = $menu->Submenus;

                $menuData = [];
                // if($this->menuObj->find(['customer_id'=>$venueID]))
                //     return redirect()->back()->with(['fail'=>'You\'ve a Menu with the same name!']);
                $menuData['customer_id'] = $venueID;
                $menuData['parent_id'] =  $request->parent_id ??  0;
                $menuData['section_id'] = $menu->section_id;
                $menuData['name'] = $menu->name . 'Copy';
                $menuData['description'] = $menu->description;
                $menuData['display_as'] = $menu->display_as;
                $menuData['num_of_rows'] = $menu->num_of_rows;
                $menuData['grid_title_position'] = $menu->grid_title_position;
                $menuData['display_similar_items'] = $menu->display_similar_items;
                $menuData['mark_as_new'] = $menu->mark_as_new;
                $menuData['mark_as_signature'] = $menu->mark_as_signature;
                $menuData['additional_notes'] = $menu->additional_notes;
                $menuData['media_id'] = $menu->media_id;
                $menuData['video_id'] = $menu->video_id;
                $menuData['is_active'] = $menu->is_active;

                $newMenu = $this->menuObj->insert($menuData);
                if($newMenu) {
                    $this->arrangmentObj->copy('Menu' , $menu->id);
                    if(count($sections)) {
                        foreach($sections as $section) {
                            $sectionData = [];
                            $sectionData['menu_id'] = $newMenu->id;
                            $sectionData['title'] = $section->title;
                            $newSection = $this->menuSectionObj->insert($sectionData);

                            if($newSection && count($section->Submenus)) {

                                foreach($section->Submenus as $submenu) {
                                  $subMenuData = [];
                                  $subMenuData['customer_id'] = $venueID;
                                  $subMenuData['parent_id'] = $newMenu->id;
                                  $subMenuData['section_id'] = $newSection->id;
                                  $subMenuData['name'] = $submenu->name;
                                  $subMenuData['description'] = $submenu->description;
                                  $subMenuData['display_as'] = $submenu->display_as;
                                  $subMenuData['num_of_rows'] = $submenu->num_of_rows;
                                  $subMenuData['grid_title_position'] = $submenu->grid_title_position;
                                  $subMenuData['display_similar_items'] = $submenu->display_similar_items;
                                  $subMenuData['mark_as_new'] = $submenu->mark_as_new;
                                  $subMenuData['mark_as_signature'] = $submenu->mark_as_signature;
                                  $subMenuData['additional_notes'] = $submenu->additional_notes;
                                  $subMenuData['media_id'] = $submenu->media_id;
                                  $subMenuData['video_id'] = $submenu->video_id;
                                  $subMenuData['is_active'] = $submenu->is_active;

                                  $newSubmenu = $this->menuObj->insert($subMenuData);

                                  if($newSubmenu) {
                                        $this->arrangmentObj->copy('Menu' , $submenu->id);
                                      if(count($submenu->items)) {
                                          foreach($submenu->items as $item) {
                                              $this->copyItem($item , $newSubmenu->id);
                                          }
                                      }
                                  }
                                }
                            }
                        }
                    } else {
                      if(count($oldSubmenus)) {
                          foreach($oldSubmenus as $submenu) {
                              $subMenuData = [];
                              $subMenuData['customer_id'] = $venueID;
                              $subMenuData['parent_id'] = $newMenu->id;
                              $subMenuData['section_id'] = $submenu->section_id;
                              $subMenuData['name'] = $submenu->name;
                              $subMenuData['description'] = $submenu->description;
                              $subMenuData['display_as'] = $submenu->display_as;
                              $subMenuData['num_of_rows'] = $submenu->num_of_rows;
                              $subMenuData['grid_title_position'] = $submenu->grid_title_position;
                              $subMenuData['display_similar_items'] = $submenu->display_similar_items;
                              $subMenuData['mark_as_new'] = $submenu->mark_as_new;
                              $subMenuData['mark_as_signature'] = $submenu->mark_as_signature;
                              $subMenuData['additional_notes'] = $submenu->additional_notes;
                              $subMenuData['media_id'] = $submenu->media_id;
                              $subMenuData['video_id'] = $submenu->video_id;
                              $subMenuData['is_active'] = $submenu->is_active;

                              $newSubmenu = $this->menuObj->insert($subMenuData);

                              if($newSubmenu) {
                                    $this->arrangmentObj->copy('Menu' , $submenu->id);
                                    if(count($submenu->items)) {
                                      foreach($submenu->items as $item) {

                                          $oldSideDishes = $item->sideDishes;
                                          $oldIngredients = $item->ingredientWarnings;
                                          $oldPrices = $item->priceList;
                                          $oldOptions = $item->itemOptions;

                                          $itemData = [];
                                          $itemData['menu_id'] = $newSubmenu->id;
                                          $itemData['name'] = $item->name;
                                          $itemData['description'] = $item->description;
                                          $itemData['portion'] = $item->portion;
                                          $itemData['preparation_time'] = $item->preparation_time;
                                          $itemData['calories'] = $item->calories;
                                          $itemData['notes'] = $item->notes;
                                          $itemData['image_id'] = $item->image_id;
                                          $itemData['video_id'] = $item->video_id;
                                          $itemData['is_active'] = $item->is_active;
                                          $itemData['deleted_at'] = $item->deleted_at;

                                          $newItem = $this->itemObj->insert($itemData);
                                          if($newItem) {
                                            $this->arrangmentObj->copy('Item' , $item->id);
                                              if(count($oldOptions)) {
                                                  foreach($oldOptions as $op) {
                                                      $oldOptionValues = $op->optionValues;
                                                      if(count($oldOptionValues)) {
                                                        $optionData = [];
                                                        $optionData['item_id'] = $newItem->id;
                                                        $optionData['option_id'] = $op->option_id;
                                                        $optionData['name'] = $op->name;
                                                        $optionData['type'] = $op->type;
                                                        $newItemOption = $this->itemOptionObj->insert($optionData);
                                                        if($newItemOption) {
                                                            foreach($oldOptionValues as $optionValue) {
                                                                $optionValueData = [];
                                                                $optionValueData['item_option_id'] = $newItemOption->id;
                                                                $optionValueData['value'] = $optionValue->value;

                                                                $this->itemOptionValueObj->insert($optionValueData);
                                                            }
                                                        }
                                                      }
                                                  }
                                              }
                                              if(count($oldSideDishes)) {
                                                  foreach($oldSideDishes as $sideDish) {
                                                      $sideDishData = [];
                                                      $sideDishData['item_id'] = $newItem->id;
                                                      $sideDishData['side_dish_id'] = $sideDish->side_dish_id;
                                                      $sideDishData['price'] = $sideDish->price;
                                                      $sideDishData['name'] = $sideDish->name;
                                                      $sideDishData['media_id'] = $sideDish->media_id;

                                                      $this->itemSideDishObj->insert($sideDishData);
                                                  }
                                              }
                                              if(count($oldPrices)) {
                                                  foreach($oldPrices as $price) {
                                                      $priceData = [];
                                                      $priceData['item_id'] = $newItem->id;
                                                      $priceData['price'] = $price->price;
                                                      $priceData['description'] = $price->description;

                                                      $this->itemPriceObj->insert($priceData);
                                                  }
                                              }
                                              if(count($oldIngredients)) {
                                                  foreach($oldIngredients as $ingredient) {
                                                      $ingredientData = [];
                                                      $ingredientData['item_id'] = $newItem->id;
                                                      $ingredientData['ingredient_warning_id'] = $ingredient->ingredient_warning_id;
                                                      $ingredientData['name'] = $ingredient->name;
                                                      $this->itemIngredientObj->insert($ingredientData);
                                                  }
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }
                    }
                }

            } else {
              return redirect()->back()->with(['fail'=>'Menu not found!']);
            }
        }
        else{
            $item = $this->itemObj->find(['id' => $request->item_id]);
            $this->copyItem($item, $item->menu_id);
        }


        return redirect()->back()->with(['success' => $request->parent_id ? 'duplicated successfully.' : 'imported successfully.']);
    }

    public function move(Request $request){
        $venueID = $request->venue_id ;
        $venue = $this->customerObj->find(['id' => $venueID]);
        if(!$venue)
          return redirect()->back()->with(['fail'=>'Venue not existed!']);
        if($request->type == 'Menu'){
            $menuID = $request->menu_id;
            $menu = $this->menuObj->find(['id'=>$menuID]);
            if($menu){
                $menu->parent_id = $request->parent_id;
                $menu->save();
                $menu->arrangment->menu_id = $menu->parent_id ;
                $menu->arrangment->save();
                return redirect()->back()->with(['success'=>'Menu Moved Successfuly']);
            }
            else{
                return redirect()->back()->with(['fail'=>'Menu not existed!']);

            }
        }
        else{
            $itemID = $request->menu_id;
            $item = $this->itemObj->find(['id'=>$itemID]);
            if($item){
                $item->menu_id = $request->parent_id;
                $item->save();
                $item->arrangment->menu_id = $item->menu_id ;
                $item->arrangment->save();
                return redirect()->back()->with(['success'=>'Item Moved Successfuly']);
            }
            else{
                return redirect()->back()->with(['fail'=>'Menu not existed!']);

            }
        }

    }

    public function destroy($id)
    {
        $this->customerObj->delete(['id' => $id]);
        return redirect()->route('diner.venue.index')->with('success', 'Customer was removed successfully');
    }

    public function copyItem($item, $menu_id){
        $oldSideDishes = $item->sideDishes;
        $oldIngredients = $item->ingredientWarnings;
        $oldPrices = $item->priceList;
        $oldOptions = $item->itemOptions;

        $itemData = [];
        $itemData['menu_id'] = $menu_id;
        $itemData['name'] = $item->name;
        $itemData['description'] = $item->description;
        $itemData['portion'] = $item->portion;
        $itemData['preparation_time'] = $item->preparation_time;
        $itemData['calories'] = $item->calories;
        $itemData['notes'] = $item->notes;
        $itemData['image_id'] = $item->image_id;
        $itemData['video_id'] = $item->video_id;
        $itemData['is_active'] = $item->is_active;
        $itemData['deleted_at'] = $item->deleted_at;

        $newItem = $this->itemObj->insert($itemData);
        if($newItem) {
            $this->arrangmentObj->copy('Item' , $item->id);
            if(count($oldOptions)) {
                foreach($oldOptions as $op) {
                    $oldOptionValues = $op->optionValues;
                    if(count($oldOptionValues)) {
                    $optionData = [];
                    $optionData['item_id'] = $newItem->id;
                    $optionData['option_id'] = $op->option_id;
                    $optionData['name'] = $op->name;
                    $optionData['type'] = $op->type;
                    $newItemOption = $this->itemOptionObj->insert($optionData);
                    if($newItemOption) {
                        foreach($oldOptionValues as $optionValue) {
                            $optionValueData = [];
                            $optionValueData['item_option_id'] = $newItemOption->id;
                            $optionValueData['value'] = $optionValue->value;

                            $this->itemOptionValueObj->insert($optionValueData);
                        }
                    }
                    }
                }
            }
            if(count($oldSideDishes)) {
                foreach($oldSideDishes as $sideDish) {
                    $sideDishData = [];
                    $sideDishData['item_id'] = $newItem->id;
                    $sideDishData['side_dish_id'] = $sideDish->side_dish_id;
                    $sideDishData['price'] = $sideDish->price;
                    $sideDishData['name'] = $sideDish->name;
                    $sideDishData['media_id'] = $sideDish->media_id;

                    $this->itemSideDishObj->insert($sideDishData);
                }
            }
            if(count($oldPrices)) {
                foreach($oldPrices as $price) {
                    $priceData = [];
                    $priceData['item_id'] = $newItem->id;
                    $priceData['price'] = $price->price;
                    $priceData['description'] = $price->description;

                    $this->itemPriceObj->insert($priceData);
                }
            }
            if(count($oldIngredients)) {
                foreach($oldIngredients as $ingredient) {
                    $ingredientData = [];
                    $ingredientData['item_id'] = $newItem->id;
                    $ingredientData['ingredient_warning_id'] = $ingredient->ingredient_warning_id;
                    $ingredientData['name'] = $ingredient->name;
                    $this->itemIngredientObj->insert($ingredientData);
                }
            }
        }
    }

    public function generateQrCode(Request $request)
    {
        $mainBranch = $this->data['current_user']->mainBranch();
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $customer = $this->customerObj->find(['id' => $customerID]);

        $credentials = [
            'branch_id' => $customer->id,
            'email' => $customer->email
        ];
        //$pngImage = \QrCode::format('png')->merge('http://localhost/menioo//public/images/Penguin.png', 0.3, true)->size(500)->generate(json_encode($credentials));
        //return response($pngImage)->header('Content-type','image/png');

        return view('qrCode')->with(['credentials' => $credentials]);
    }
}
