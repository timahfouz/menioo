<?php

namespace App\Http\Controllers\Diner;

use App\Http\Requests\Common\LoginRequest;
use App\Http\Services\Diner\DinerLoginService;
use Illuminate\Support\Facades\Auth;
Use URL;

class DinerLoginController extends DinerController
{
    private $dinerLoginObj;

    public function __construct(DinerLoginService $dinerLoginObj)
    {
        $this->dinerLoginObj = $dinerLoginObj;
    }

    public function login()
    {
        if (!Auth::guard('diner')->user()) {
            $this->data['title'] = (object)['name' => 'Login'];
            return view('diner.auth.login', $this->data);
        }
        return redirect()->route('diner.menu.index');
    }

    public function doLogin(LoginRequest $request)
    {
        $credentials = $request->except(['_token', 'remember']);

        $isLogin = $this->dinerLoginObj->login($credentials, $request->remember);

        if ($isLogin) {
            return redirect()->route('diner.menu.index')->with(['success' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    public function logout()
    {
        $this->dinerLoginObj->logout();
        return redirect()->route('landing.login');
    }
}
