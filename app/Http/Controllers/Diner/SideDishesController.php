<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\SideDishService;
use Auth;
use App\Http\Services\Common\Helper;

class SideDishesController extends DinerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $sidedish;
    private $helperObj;

    public function __construct(Helper $helperObj, SideDishService $sidedish)
    {
        parent::__construct();
        $this->sidedish = $sidedish;
        $this->helperObj = $helperObj;
    }
    public function index(Request $request)
    {
        //
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['venue_id'] = $customerID;
        $this->data['title'] = (object)[
            'name' => 'App Modifiers'
        ];
        
        $this->data['sideDishes'] = $this->sidedish->getAll(['customer_id' => $customerID])->map(function($item){
            $item->label = $item->name;
            $item->value = $item->name;
            $item->photo = $item->FullThumbnailImagePath('300X300');
            return $item;
        });
        
        return view('diner.configration.modifiers',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $data = $request->except(['_token', 'image']);

        $sideDishitem = $this->sidedish->find(['name' => $data['name']]);

        if ($request->has('image')) {

            $media = $this->helperObj->upload($request->file('image'), ['title' => $data['name'], 'customer_id' => $customerID]);

            $data['media_id'] = $media->id;
        }
        $data['customer_id'] = $customerID;
        if($sideDishitem){
            $sideDishitem = $this->sidedish->update(['name' => $data['name']],$data);
        }
        else{
            $sideDishitem = $this->sidedish->insert($data);

        }

        if ($sideDishitem) {
            return redirect()->route('diner.sidedishes.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleted = $this->sidedish->delete(['id' => $id]);
        if($deleted){
            return ['success' => 'SideDish was removed successfully'];

        }
        else{
            return ['Error' => 'SideDish Faild to delete'];

        }
        return redirect()->route('diner.sidedishes.index')->with(['success' => 'SideDish was removed successfully']);


    }

}
