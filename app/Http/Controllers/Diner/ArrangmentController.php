<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Diner\MenuService;


class ArrangmentController extends DinerController
{
    //
    private $arrangmentObj;
    private $menuObj;
    public function __construct(ArrangmentService $arrangmentObj,MenuService $menuObj)
    {
        parent::__construct();
        $this->arrangmentObj = $arrangmentObj;
        $this->menuObj = $menuObj;
    }
    public function arrange(Request $request,$id){
        $menu = $this->menuObj->find(['id' =>$id]);
        parse_str($request->data,$newOrders);
        $newOrders = array_map('intval',$newOrders['card']);
        $ordered = [];
        foreach($newOrders as $key => $value){
            $newObj = $this->arrangmentObj->find(['id' => $value]);
            $newObj->order = $key +1;
            $newObj->save();
        }
        return ['success' => 'Arrangment Done Successfully'];

        
    }

    
}
