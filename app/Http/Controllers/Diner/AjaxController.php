<?php

namespace App\Http\Controllers\Diner;

use App\Http\Controllers\Controller;
use App\Http\Services\Common\CountryService;
use App\Http\Services\Common\Helper;
use App\Http\Services\Diner\MenuSectionsService;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\SideDishService;
use App\Http\Services\Diner\MenuTranslationService;
use App\Http\Services\Admin\SubscriptionServices;
use App\Http\Services\Admin\CustomerServices;

use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;


class AjaxController extends Controller
{

    private $countryObj;
    private $menuObj;
    private $menuSectionObj;
    private $itemObj;
    private $helperObj;
    private $SideDishObj;
    private $menuTransObj;
    private $planObj;
    private $customerObj;


    public function __construct(SideDishService $SideDishObj, CountryService $countryObj, MenuService $menuObj, MenuSectionsService $menuSectionObj, ItemService $itemObj, Helper $helperObj, MenuTranslationService $menuTransObj, CustomerServices $customerObj, SubscriptionServices $planObj)
    {
        $this->countryObj = $countryObj;
        $this->menuObj = $menuObj;
        $this->menuSectionObj = $menuSectionObj;
        $this->itemObj = $itemObj;
        $this->helperObj = $helperObj;
        $this->SideDishObj = $SideDishObj;
        $this->menuTransObj = $menuTransObj;
        $this->customerObj = $customerObj;
        $this->planObj = $planObj;
    }

    public function validation(Request $request)
    {
        $conditions = array();
        $service = $request->model . 'Obj';

        if ($request->id) {
            $conditions[] = ['id', '!=', $request->id];
        }

        if ($request->name) {
            if($request->model == 'menu') {

                $conditions['customer_id'] = Auth::guard('diner')->user()->id;
                $conditions['parent_id'] = $request->parent_id;
                $inTranslation = $this->menuTransObj->getAll(['name' => $request->name])->pluck('menu_id')->toArray();
                $model_exists = $this->$service->findIn($conditions, $inTranslation);

            } else {
                $conditions['name'] = $request->name;
                $conditions['customer_id'] = Auth::guard('diner')->user()->id;
                $conditions['parent_id'] = $request->parent_id;
                $model_exists = $this->$service->find($conditions);
            }
            
            if ($model_exists) {

                $response = array('valid' => false, 'message' => 'This name is already taken.');
            } else {

                $response = array('valid' => true);
            }
            echo json_encode($response);

        }
    }

    public function HideShowMenu(Request $request)
    {
        $response = array('valid' => true);
        $current_customer = Auth::guard('diner')->user();
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        }

        else {
            $id = $request->id;
            $current_menu = $this->menuObj->find(['id' => $id]);
            if ($current_menu) {
                $is_active = $current_menu->is_active == 0 ? 1 : 0;
                $data = $this->menuObj->update(['id' => $id], ['is_active' => $is_active]);
                if ($data) {
                    $response = array('valid' => true, 'status' => 1, 'data' => $this->menuObj->find(['id' => $id]));
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                }
            } else {
                $response = array('valid' => false, 'status' => 2, 'error' => 'Menu Not Found');
            }
        }

        echo json_encode($response);
    }

    public function HideShowItem(Request $request)
    {
        $response = array('valid' => true);
        $current_customer = Auth::guard('diner')->user();
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        } else {
            $id = $request->id;
            $current_item = $this->itemObj->find(['id' => $id]);
            if ($current_item) {
                $is_active = $current_item->is_active == 0 ? 1 : 0;
                $data = $this->itemObj->update(['id' => $id], ['is_active' => $is_active]);
                if ($data) {
                    $response = array('valid' => true, 'status' => 1, 'data' => $this->itemObj->find(['id' => $id]));
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                }
            } else {
                $response = array('valid' => false, 'status' => 2, 'error' => 'Menu Not Found');
            }
        }
        echo json_encode($response);
    }

    public function cities(Request $request)
    {
        $html = '';
        if ($request->id) {
            $cities = $this->countryObj->getAll(['parent_id' => $request->id]);
            if (count($cities)) {
                foreach ($cities as $city) {
                    $html .= '<option value="' . $city->id . '">' . $city->name . '</option>';
                }
            }
        }
        return $html;
    }


    public function upload(Request $request)
    {
        $this->helperObj->upload($request->file('file'));
    }

    public function sectionCreate(Request $request)
    {
        $id = ($request->id ? $request->id : 'null');
        $validator = Validator::make($request->all(), [
            'menu_id' => 'required|numeric',
            'title' => 'required|unique:menu_sections,title,' . $id . ',id,menu_id,' . $request->menu_id,
        ]);

        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        } else {
            if (is_numeric($id)) {
                $current_section = $this->menuSectionObj->find(['id' => $id, 'menu_id' => $request->menu_id]);
                if ($current_section) {
                    $data = $this->menuSectionObj->update(['id' => $id, 'menu_id' => $request->menu_id], ['title' => $request->title]);
                    if ($data) {
                        $response = array('valid' => true, 'status' => 1, 'data' => $data, 'message' => 'Updated Successfully');
                    } else {
                        $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                    }
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Section Not Found');
                }
            } else {
                $data = $this->menuSectionObj->insert(['menu_id' => $request->menu_id, 'title' => $request->title]);
                $this->menuObj->update(['parent_id' => $request->menu_id, 'section_id' => null, 'customer_id' => Auth::guard('diner')->user()->id],
                    ['section_id' => $data->id]);

                if ($data) {
                    $this->menuObj->update(['parent_id' => $id, 'section_id' => 'null'], ['section_id' => $data->id]);
                    $response = array('valid' => true, 'status' => 1, 'data' => $data, 'message' => 'Added Successfully');
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                }
            }
        }

        echo json_encode($response);
    }

    public function sectionRemove(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'menu_id' => 'required|numeric',
            'id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        } else {
            $id = $request->id;
            $current_section = $this->menuSectionObj->find(['id' => $id, 'menu_id' => $request->menu_id]);
            if ($current_section) {
                $last_section = $this->menuSectionObj->getLastOne(['menu_id' => $request->menu_id, ['id', '!=', $id]]);
                if ($last_section) {
                    $this->menuObj->update(['parent_id' => $request->menu_id, 'section_id' => $id, 'customer_id' => Auth::guard('diner')->user()->id],
                        ['section_id' => $last_section->id]);
                } else {
                    $this->menuObj->update(['parent_id' => $request->menu_id, 'section_id' => $id, 'customer_id' => Auth::guard('diner')->user()->id],
                        ['section_id' => null]);
                }
                $data = $this->menuSectionObj->delete(['id' => $id, 'menu_id' => $request->menu_id]);
                if ($data) {
                    $response = array('valid' => true, 'status' => 1, 'data' => $data, 'message' => 'Deleted Successfully');
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                }
            } else {
                $response = array('valid' => false, 'status' => 2, 'error' => 'Section Not Found');
            }
        }

        echo json_encode($response);
    }

    public function editCustomerPlan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        } else {
            $planID = $request->plan_id;
            if(Auth::guard('diner')->check()) {
                $customerID = Auth::guard('diner')->user()->id;
            } else {
                $customerID = 0;
            }
            $plan = $this->planObj->find(['plan_id' => $planID]);
            if ($plan) {
                $this->customerObj->update(['id' => $customerID],['plan_id' => $plan->id]);
                $response = array('status' => true, 'message' => 'done.');
            } else {
                $response = array('status' => false, 'message' => 'error.', 'error' => 'Customer or Plan Not Found');
            }
        }
        return json_encode($response);
    }
}
