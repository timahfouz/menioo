<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\OptionService;
use Auth;
use App\Http\Services\Common\Helper;

class OptionController extends DinerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $option;
    private $helperObj;

    public function __construct(Helper $helperObj, OptionService $option)
    {
        parent::__construct();
        $this->option = $option;
        $this->helperObj = $helperObj;
    }
    public function index(Request $request)
    {
        //
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : Auth::guard('diner')->user()->id;

        $data = $request->except(['_token', 'image']);
        $option = $this->option->find(['name' => $data['name']]);

        
        $data['customer_id'] = $customerID;
        if($option){
            $option = $this->option->update(['name' => $data['name']],$data);
        }
        else{
            $option = $this->option->insert($data);

        }

        if ($option) {
            return redirect()->route('diner.sidedishes.index')->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleted = $this->option->delete(['id' => $id]);
        if($deleted){
            return ['success' => 'SideDish was removed successfully'];

        }
        else{
            return ['Error' => 'SideDish Faild to delete'];

        }
        return redirect()->route('diner.sidedishes.index')->with(['success' => 'SideDish was removed successfully']);


    }

    public function getoption($id){
        $option = $this->option->find(['id' => $id]);
        if($option){
            return $option;
        }
        else{
            return [];
        }
    }

}
