<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemService;
use App\Http\Services\Diner\FeedbackService;
use App\Http\Services\Diner\ArrangmentService;
use App\Http\Services\Diner\MenuTranslationService;
use App\Http\Services\Diner\ItemTranslationService;
use App\Http\Services\Diner\QuestionTranslationService;
use App\Http\Models\Configration;


class LocalizationController extends DinerController
{
    //
    private $menuObj;
    private $itemObj;
    private $feedbackObj;
    private $arangeObj;
    private $menuTranslationObj;
    private $itemTranslationObj;
    private $surveyTranslationObj;
    public function __construct(QuestionTranslationService $surveyTranslationObj, FeedbackService $feedbackObj, MenuService $menuObj, ItemService $itemObj, ArrangmentService $arangeObj, MenuTranslationService $menuTranslationObj, ItemTranslationService $itemTranslationObj)
    {
        parent::__construct();
        $this->menuObj = $menuObj;
        $this->itemObj = $itemObj;
        $this->feedbackObj = $feedbackObj;
        $this->arangeObj = $arangeObj;
        $this->menuTranslationObj = $menuTranslationObj;
        $this->itemTranslationObj = $itemTranslationObj;
        $this->surveyTranslationObj = $surveyTranslationObj;
    }
    public function index(Request $request){
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['Main_menus'] = $this->menuObj->getAll(['parent_id' => 0, 'customer_id' => $customerID]);
        $this->data['json_menu_data'] =json_encode(
            $this->data['Main_menus']->map(function($menu){
                $tree['text'] = str_replace("'",'-',$menu->name);
                $tree['id'] = $menu->id;
                $tree['type'] = 'Menu';
                $tree['nodes'] = $this->listmeueNodes($menu,[]);
                return $tree;
            })
        );
        $this->data['survey_forms'] = $this->feedbackObj->getAll([]);
        $this->data['venue_id'] = $customerID;
        $this->data['title'] = (object)[
            'name' => 'Localization'
        ];
        return view('diner.localization.index',$this->data);
    }

    public function listmeueNodes($menu, $nodes){
        if(!count($menu->SubMenus)){
            if(!count($menu->items))
                return [];
            else{
                $itemArr =[];
                foreach ($menu->items as $item) {
                    $obj['text'] = str_replace("'",'-',$item->name);
                    $obj['id'] = $item->id;
                    $obj['type'] = 'Item';
                    $itemArr[] =$obj;
                }
                return $itemArr;
            }
        }
        else{
            foreach ($menu->SubMenus as $submenu) {
                $menuNode = [
                    'text' => str_replace("'",'-',$submenu->name),
                    'id' => $submenu->id,
                    'type' => 'Menu',
                    'nodes' => $this->listmeueNodes($submenu,[])
                ];
                foreach ($submenu->items as $item) {
                    $obj['text'] = str_replace("'",'-',$item->name);
                    $obj['id'] = $item->id;
                    $obj['type'] = 'Item';
                    $menuNode['nodes'][] =$obj;
                }
                array_push($nodes,$menuNode);
            }
            return $nodes;

        }
    }

    public function getTranslation($id, $type){
        $obj = ($type == 'Menu') ?
                        $this->menuObj->find(['id' => $id]) :
                        $this->itemObj->find(['id' => $id]  ) ;
        return $obj;               
    }


    public function updatelanguage(Request $request){
        if($request->data['type'] == 'Menu'){
            $menu = $this->menuTranslationObj->find(['menu_id' => $request->data['id'], 'locale' => $request->data['lang']]);
            if($menu){
                $menu[$request->data['col']] = $request->data['value'];
                $menu->save();
            }
            else{
                $this->menuTranslationObj->insert([
                    'menu_id' => $request->data['id'],
                    $request->data['col'] => $request->data['value'],
                    'locale' => $request->data['lang']
                ]);
            }
        }
        else{
            $item = $this->itemTranslationObj->find(['item_id' => $request->data['id'], 'locale' => $request->data['lang']]);
            if($item){
                $item[$request->data['col']] = $request->data['value'];
                $item->save();
            }
            else{
                $this->itemTranslationObj->insert([
                    'item_id' => $request->data['id'],
                    $request->data['col'] => $request->data['value'],
                    'locale' => $request->data['lang'],
                ]);
            }
        }
        return "Updated Successfully";

    }

    public function updatesurveylanguage(Request $request){
        $survey = $this->surveyTranslationObj->find(['question_id' => $request->data['id'], 'locale' => $request->data['lang']]);
        if($survey){
            $survey->value = $request->data['value'];
            $survey->save();
        }
        else{
            $this->surveyTranslationObj->insert([
                'question_id' => $request->data['id'],
                'value' => $request->data['value'],
                'locale' => $request->data['lang']
            ]);
        }
        return "Updated Successfully";

    }
}
