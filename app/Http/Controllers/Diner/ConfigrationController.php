<?php

namespace App\Http\Controllers\Diner;

use Illuminate\Http\Request;
use App\Http\Services\Diner\ConfigrationService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Models\Configration;


class ConfigrationController extends DinerController
{
    private $configObj;
    private $customerObj;
    public function __construct(CustomerServices $customerObj, ConfigrationService $configObj)
    {
        parent::__construct();
        $this->configObj = $configObj;
        $this->customerObj = $customerObj;
    }
    public function index(Request $request){
        $customerID = (int)$request->venue_id ? (int)$request->venue_id : $this->data['current_user']->id;
        $this->data['venue_id'] = $customerID;
        $this->data['title'] = (object)[
            'name' => 'App Cofigrations'
        ];

        $this->data['config'] = $this->configObj->find(['customer_id' => $customerID]) ?? new Configration();
        $this->data['config']['waiter_login'] = $this->customerObj->find(['id' => $customerID])->waiter_login;

        return view('diner.configration.index', $this->data);
    }

    public function store(Request $request){
        $customerID = (int)$request->customer_id ? (int)$request->customer_id : $this->data['current_user']->id;
        $this->data['venue_id'] = $customerID;
        $data = $request->except(['_token', '_method']);
        $waiter_login = isset($data['waiter_login']) ? 1 : 0;
        $customer = $this->customerObj->find(['id' => $customerID]);
        $customer->waiter_login = $waiter_login;
        $customer->save();
        unset($data['waiter_login']);
        $data['other_languages'] = json_encode ($data['other_languages']);
        $data['show_language_icon'] = isset($data['show_language_icon']) ? 1 : 0;
        $data['show_info_icon'] = isset($data['show_info_icon']) ? 1 : 0;
        $data['show_feedback_icon'] = isset($data['show_feedback_icon']) ? 1 : 0;
        $data['show_labels'] = isset($data['show_labels']) ? 1 : 0;
        $data['customer_id'] = $customerID;
        $data['emails'] = ($data['emails'][0]) ? json_encode ($data['emails']) : '';
        $this->configObj->delete(['customer_id' => $customerID]);
        $config = $this->configObj->updateOrCreate(['customer_id' => $customerID],$data);
        if ($config) {
            return redirect()->back()->with(['success' => 'Updated successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }
}
