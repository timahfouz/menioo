<?php
/**
 * Created by PhpStorm.
 * User: Tarek
 * Date: 7/29/2019
 * Time: 4:46 PM
 */

use App\Locale;

function locale()
{
    return app()->make(Locale::class);
}
