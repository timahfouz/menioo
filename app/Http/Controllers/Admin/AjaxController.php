<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\Common\CountryService;
use App\Http\Services\Admin\CustomerServices;
use Validator;
use Illuminate\Http\Request;


class AjaxController extends Controller
{

    private $countryObj;
    private $customerObj;

    public function __construct(CountryService $countryObj, CustomerServices $customerObj)
    {
        $this->countryObj = $countryObj;
        $this->customerObj = $customerObj;
    }

    public function validation(Request $request)
    {
        $conditons = array();

        $service = $request->model . 'Obj';
        if ($request->id) {
            $conditons[] = ['id', '!=', $request->id];
        }

        if ($request->email) {
            $conditons['email'] = $request->email;

            $model_exists = $this->$service->find($conditons);

            if ($model_exists) {

                $response = array('valid' => false, 'message' => 'This email is already taken.');
            } else {

                $response = array('valid' => true);
            }
            echo json_encode($response);

        } elseif ($request->mobile_number) {

            $conditons['mobile_number'] = $request->mobile_number;

            $model_exists = $this->$service->find($conditons);

            if ($model_exists) {

                $response = array('valid' => false, 'message' => 'This mobile is already taken.');
            } else {

                $response = array('valid' => true);
            }
            echo json_encode($response);
        } elseif ($request->name && $request->lang_code && $request->lang_id) {
            $conditons['name'] = $request->name[$request->lang_code];
            $conditons['lang_id'] = $request->lang_id;
            $model_exists = $this->$service->find($conditons);

            if ($model_exists) {
                $response = array('valid' => false, 'message' => 'This ' . $request->model . ' name is already taken.');
            } else {

                $response = array('valid' => true);
            }
            echo json_encode($response);
        }
        
    }

    public function cities(Request $request)
    {
        $html = '';
        if ($request->id) {
            $cities = $this->countryObj->getAll(['parent_id' => $request->id]);
            if (count($cities)) {
                foreach ($cities as $city) {
                    $html .= '<option value="' . $city->id . '">' . $city->name . '</option>';
                }
            }
        }
        return $html;
    }

    public function activeDeactivate(Request $request)
    {
        $response = array('valid' => true);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $response = array('valid' => false, 'status' => 2, 'error' => implode(",", $validator->messages()->all()));
        } else {
            $id = $request->id;
            $current_customer = $this->customerObj->find(['id' => $id]);
            if ($current_customer) {
                $is_active = $current_customer->is_active == 0 ? 1 : 0;
                $data = $this->customerObj->update(['id' => $id], ['active_deactivate_date' => date('Y-m-d H:i:s'), 'is_active' => $is_active]);
                if ($data) {
                    $response = array('valid' => true, 'status' => 1, 'data' => $this->customerObj->find(['id' => $id]));
                } else {
                    $response = array('valid' => false, 'status' => 2, 'error' => 'Whooops something is wrong');
                }
            } else {
                $response = array('valid' => false, 'status' => 2, 'error' => 'Customer Not Found');
            }
        }

        echo json_encode($response);
    }
}