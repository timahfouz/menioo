<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CustomerRequest;
use App\Http\Services\Common\CountryService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;
use Illuminate\Http\Request;
Use URL;

class CustomerController extends AdminController
{
    private $customerObj;
    private $countryObj;
    private $helperObj;

    public function __construct(CustomerServices $customerObj, CountryService $countryObj, Helper $helperObj)
    {
        parent::__construct();
        $this->customerObj = $customerObj;
        $this->helperObj = $helperObj;
        $this->countryObj = $countryObj;
    }

    public function index(Request $request)
    {
        $conditions = [['id', '>', 0],['parent_id', '=', 0]];

        if ($request->keyword) {
            $conditions[] = ['name', 'like', '%' . $request->keyword . '%'];
        }

        if ($request->is_active != '') {
            $conditions['is_active'] = $request->is_active;
        }

        if ($request->country_id) {
            $conditions['country_id'] = $request->country_id;
        }

        if ($request->city_id) {
            $conditions['city_id'] = $request->city_id;
        }

        $per_page = $request->per_page ? $request->per_page : 20;
        $this->data['customers'] = $this->customerObj->getAll($conditions, $per_page);
        $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);
        $total_count = $this->customerObj->getTotalCount(['parent_id' => 0]);

        $this->data['title'] = (object)['name' => 'Customers', 'url' => URL::to('admin/customer'),
            'additional' => (object)['name' => 'Export', 'url' => 'javascript:void(0)', 'class' => 'plain-btn'],
            'child' => (object)['name' => 'Create Customer', 'url' => URL::to('admin/customer/create'), 'subtitle' => $total_count . ' Customers']
        ];

        //return $this->data;
        return view('admin.customer.index', $this->data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = (object)['name' => 'Create Customer', 'url' => URL::to('admin/customer'),
            'parent' => (object)['name' => 'Customer']];
        $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

        return view('admin.customer.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(CustomerRequest $request)
    {
        $data = $request->except(['_token']);

//        if ($request->hasFile('profile_pic')) {
//            $data['profile_pic'] = $this->helperObj->upload($request->file('profile_pic'));
//        }

        $vendor = $this->customerObj->insert($data);

        if ($vendor) {
            return redirect()->route('admin.customer.edit', ['id' => $vendor->id])->with(['success' => 'added successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['current_customer'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_customer']) {
            $this->data['title'] = (object)['name' => 'Customer Details', 'url' => URL::to('admin/customer'),
                'additional' => (object)['name' => 'Login AS ' . $this->data['current_customer']->name, 'url' => URL::to('admin/login/customer/' . $this->data['current_customer']->id), 'class' => 'white-btn'],
                'child' => (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer/' . $this->data['current_customer']->id . '/edit'), 'subtitle' => $this->data['current_customer']->name]
            ];
            $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

            return view('admin.customer.show', $this->data);
        } else {
            return view('admin.404');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['current_customer'] = $this->customerObj->find(['id' => $id]);
        if ($this->data['current_customer']) {
            $this->data['title'] = (object)['name' => 'Edit Customer', 'url' => URL::to('admin/customer'),
                'parent' => (object)['name' => 'Customer']];
            $this->data['countries'] = $this->countryObj->getAll([['id', '>', 0]]);

            return view('admin.customer.edit', $this->data);
        } else {
            return view('admin.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(CustomerRequest $request, $id)
    {
        $current_user = $this->customerObj->find(['id' => $id]);
        if ($current_user) {
            $data = $request->except(['_token', '_method']);

//            if ($request->hasFile('profile_pic') && $current_user->profile_pic != '') {
//                $data['profile_pic'] = $this->helperObj->upload($request->file('profile_pic'), 'edit', $current_user->profile_pic);
//            }

            $current_vendor = $this->customerObj->update(['id' => $id], $data);

            if ($current_vendor) {
                return redirect()->back()->with(['success' => 'updated successfully']);
            } else {
                return redirect()->back()->with(['fail' => 'whoops something wrong']);
            }
        } else {
            return view('admin.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customerObj->delete(['id' => $id]);
        return redirect()->route('admin.customer.index')->with('success', 'Customer was removed successfully');
    }

}
