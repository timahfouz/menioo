<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Diner\DinerController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CustomerRequest;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\CountryService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

/*
	** REQUESTS
*/
use App\Http\Requests\Common\LoginRequest;


/*
	** SERVICES
*/
use App\Http\Services\Diner\DinerLoginService;
use App\Http\Services\Admin\SubscriptionServices;
use Laravel\Socialite\Facades\Socialite;
use phpseclib\Crypt\Hash;


class LandingController extends DinerController
{
    public $data;
    private $customerObj;
    private $dinerLoginObj;
    private $planObj;
    private $countryObj;

    public function __construct(CustomerServices $customerObj, DinerLoginService $dinerLoginObj, SubscriptionServices $planObj, CountryService $countryObj)

    {
        parent::__construct();
        /*$this->middleware(function ($request, $next) {
            if (Auth::guard('diner')->check()) {
            	$this->data['user'] = Auth::guard('diner')->user();
            }
            return $next($request);
        });*/
        $this->dinerLoginObj = $dinerLoginObj;
        $this->customerObj   = $customerObj;
        $this->planObj   = $planObj;
        $this->countryObj = $countryObj;
    }

    public function index(Request $request)
    {
        return view('landing.index');
    }
    public function login(Request $request)
    {
        if(Auth::guard('diner')->guest())
            return view('landing.login');

        return redirect('diner/menu');
    }
    public function submitLogin(LoginRequest $request)
    {
        $credentials = $request->except(['_token', 'remember']);

        $isLogin = $this->dinerLoginObj->login($credentials, $request->remember);

        if ($isLogin) {
            return redirect()->route('landing.pricing')->with(['success' => 'Logged in successfully']);
        } else {
            return redirect()->back()->with('fail', 'Whoops something wrong');
        }
    }

    public function signUp(Request $request)
    {
        if(Auth::guard('diner')->guest())
            return view('landing.sign-up');
        return redirect('diner/menu');
    }
    public function submitSigning(CustomerRequest $request)
    {
        # Deny Free Plan Editing (Admin Controller)
        $data = $request->except(['_token']);
        $loginData = $request->only(['email','password']);
        $data['plane_id'] = 1;

        $token = '_'.time().Str::random(10);
        $data['email_token'] = $token;
        $this->customerObj->insert($data);

        $logged = $this->dinerLoginObj->login($loginData, 1);
        if($logged) {
            $from = env('MAIL_USERNAME');
            Mail::send('mails.verify-mail', $data, function ($mail) use($data, $from) {
                $mail->from($from, 'Menioo');
                $mail->to($data['email'], 'Menioo')->subject('Verify your email!');
            });
            return redirect()->route('landing.pricing')->with(['success' => 'Logged in successfully']);
        } else
            return redirect()->back()->with('fail', 'Whoops something wrong');
    }

    public function features(Request $request)
    {
        return view('landing.features');
    }
    public function blog(Request $request)
    {
        return view('landing.blog');
    }
    public function blog1(Request $request)
    {
        return view('landing.blog1');
    }
    public function blog2(Request $request)
    {
        return view('landing.blog2');
    }
    public function blog3(Request $request)
    {
        return view('landing.blog3');
    }

    public function pricing(Request $request)
    {
        $conditions = [];
        if($request->filled('type')) {
            $conditions['billing_period'] = $request->type;
        } else {
            $conditions['billing_period'] = 12;
        }
        $plans = $this->planObj->getAll($conditions);
        return view('landing.pricing', compact('plans'));
    }
    public function terms(Request $request)
    {
        return view('landing.terms');
    }
    public function about(Request $request)
    {
        return view('landing.about');
    }
    public function privacy(Request $request)
    {
        return view('landing.privacy');
    }
    public function forgetPassword(Request $request)
    {
        return view('landing.forget-password');
    }
    public function sendForgetPasswordRequest(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:customers,email'
        ]);
        $token = '_'.time().Str::random(10);
        $data = $request->all();
        $data['token'] = $token;
        $this->customerObj->update(['email' => $data['email']],['email_token' => $token]);

        $from = env('MAIL_USERNAME');
        Mail::send('mails.reset-password', $data, function ($mail) use($data, $from) {
            $mail->from($from, 'Menioo');
            $mail->to($data['email'], 'Menioo')->subject('Reset Password Request!');
        });
        session()->flash('success', 'Check your Email');
        return redirect()->back();
    }

    public function resetPasswordView(Request $request)
    {
        $token = $request->token;
        return view('landing.reset-password',compact('token'));
    }
    public function resetPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8|confirmed'
        ]);
        $password = $request->password;
        $token = $request->token;
        $customer = $this->customerObj->find(['email_token' => $token]);
        if ($customer) {
            $this->customerObj->update(['email_token' => $token], ['password' => bcrypt($password), 'email_token' => null]);
            $credentials = ['email' => $customer->email, 'password' => $password];
            $isLogin = $this->dinerLoginObj->login($credentials, true);
            return redirect()->to('diner/menu');
        }else {
            return redirect()->back();
        }
    }


    public function getQuote(Request $request)
    {
        $countries = $this->countryObj->getAll(['parent_id' => 0]);
        return view('landing.get-quote',compact('countries'));
    }
    public function sendQuote(Request $request)
    {
        $to = env('MAIL_USERNAME');
        $data = $request->all();
        Mail::send('mails.quote-request', $data, function ($mail) use($data, $to) {
            $mail->from('timahfouz262@gmail.com', 'Menioo');
            $mail->to($to, 'Menioo')->subject('Quote Request!');
        });
        return view('landing.quote-sent');
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        return $getInfo;
        /*$user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect()->to('/');*/
    }

    public function verifyFirst()
    {
        return view('landing.common.verify-first');
    }
    public function verify(Request $request)
    {
        $token = $request->token;
        $customer = $this->customerObj->find(['email_token' => $token]);

        if ($customer)
            $this->customerObj->update(['email_token' => $token], ['status'=> 1, 'email_token' => null]);
        return redirect()->route('landing.pricing');
    }



}

