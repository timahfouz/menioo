<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\Diner\TableService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TableController extends APIController
{
    private $tableObj;

    public function __construct(TableService $tableObj)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            $this->customer = Auth::guard('venue')->user();
            $this->customerId = $this->customer ? $this->customer->id : $this->user->customer_id;
            return $next($request);
        });

        $this->tableObj = $tableObj;
    }


    public function index(Request $request)
    {
        try {
            $statusCode = 200;
            $message = "data received";

            $customerID = $request->customer_id;
            $result = $this->tableObj->getAll(['customer_id' => $customerID]);
            $result->map(function($table) {
              $table->makeHidden(['id','customer_id']);
            });
        } catch (\Exception $e) {
            $statusCode = 500;
            $message = $e->getMessage();
            $result = [];
        } finally {
            return Controller::jsonResponse($statusCode,$message,$result);
        }

    }
}
