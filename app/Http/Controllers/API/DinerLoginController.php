<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\API\LoginRequest;
use App\Http\Services\Diner\DinerLoginService;
use Illuminate\Support\Facades\Auth;
Use URL;

class DinerLoginController extends APIController
{
    private $dinerLoginObj;

    public function __construct(DinerLoginService $dinerLoginObj)
    {
        $this->dinerLoginObj = $dinerLoginObj;
    }


    public function login(LoginRequest $request)
    {
        try {
            $statusCode = 200;
            $this->data['message'] = "data received";
            $credentials = $request->except(['_token', 'remember']);
            $isLogin = $this->dinerLoginObj->login($credentials, $request->remember);
            $this->data['user'] = $isLogin;

        } catch (\Exception $e) {
            $statusCode = 500;
            $this->data['message'] = $e->getMessage();
        } finally {
            return $this->sendResponse($this->data, $statusCode);
        }

    }

    public function logout()
    {
        $this->dinerLoginObj->logout();
        return redirect()->route('diner.login');
    }
}
