<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\Diner\MenuService;
use Illuminate\Support\Facades\Auth;
Use URL;

class SubMenuController extends APIController
{
    private $menuObj;

    public function __construct(MenuService $menuObj)
    {
        $this->menuObj = $menuObj;
    }


    public function index()
    {
        try {
            $statusCode = 200;
            $this->data['message'] = "data received";
            $this->data['menus'] = [];
            $menus = $this->menuObj->getAll([['parent_id', '=', 0], 'is_active' => 1]);
            if (count($menus)) {
                foreach ($menus as $menu) {
                    $obj = new \stdClass();
                    $obj->id = $menu->id;
                    $obj->name = $menu->name;
                    $obj->display_as = $menu->display_as;
                    $obj->num_of_rows = $menu->num_of_rows;
                    $obj->grid_title_position = $menu->grid_title_position;
                    $obj->display_similar_items = $menu->display_similar_items;
                    $obj->mark_as_new = $menu->mark_as_new;
                    $obj->mark_as_signature = $menu->mark_as_signature;
                    $obj->menu_image = $menu->FullThumbnailImagePath('335X165');
                    $this->data['menus'][] = $obj;
                }
            }

        } catch (\Exception $e) {
            $statusCode = 500;
            $this->data['message'] = $e->getMessage();
        } finally {
            return $this->sendResponse($this->data, $statusCode);
        }

    }

    public function show($id)
    {
        try {
            $menu = $this->menuObj->find(['id' => $id, 'is_active' => 1]);
            $this->data['result'] = $menu;
            if ($menu) {
                $obj = new \stdClass();
                $obj->id = $menu->id;
                $obj->name = $menu->name;
                $obj->display_as = $menu->display_as;
                $obj->num_of_rows = $menu->num_of_rows;
                $obj->grid_title_position = $menu->grid_title_position;
                $obj->display_similar_items = $menu->display_similar_items;
                $obj->mark_as_new = $menu->mark_as_new;
                $obj->mark_as_signature = $menu->mark_as_signature;
                $obj->menu_image = $menu->FullThumbnailImagePath('335X165');
                $obj->sections = [];
                $obj->sub_menus = [];
                $sections_arr = [];

                if (count($menu->sections)) {
                    foreach ($menu->sections as $section) {
                        $section_obj = new \stdClass();
                        $section_obj->id = $section->id;
                        $section_obj->title = $section->title;
                        $subMenu_arr = [];
                        if (count($section->SubMenus)) {
                            foreach ($section->SubMenus as $subMenu) {
                                $subMenu_obj = new \stdClass();
                                $subMenu_obj->id = $subMenu->id;
                                $subMenu_obj->name = $subMenu->name;
                                $subMenu_obj->mark_as_new = $subMenu->mark_as_new;
                                $subMenu_obj->mark_as_signature = $subMenu->mark_as_signature;
                                $subMenu_obj->menu_image = $subMenu->FullThumbnailImagePath('335X165');
                                $subMenu_arr[] = $subMenu_obj;
                            }
                        }
                        $section_obj->sub_menus = $subMenu_arr;
                        $sections_arr[] = $section_obj;
                    }
                    $obj->sections = $sections_arr;
                } elseif (count($menu->SubMenus)) {
                    $subMenu_arr = [];
                    foreach ($menu->SubMenus as $subMenu) {
                        $subMenu_obj = new \stdClass();
                        $subMenu_obj->id = $subMenu->id;
                        $subMenu_obj->name = $subMenu->name;
                        $subMenu_obj->mark_as_new = $subMenu->mark_as_new;
                        $subMenu_obj->mark_as_signature = $subMenu->mark_as_signature;
                        $subMenu_obj->menu_image = $subMenu->FullThumbnailImagePath('335X165');
                        $subMenu_arr[] = $subMenu_obj;
                    }
                    $obj->sub_menus = $subMenu_arr;
                }
                $this->data['result'] = $obj;
                $statusCode = 200;
            } else {
                $statusCode = 404;
            }
            $this->data['message'] = "data received";
        } catch (\Exception $e) {
            $statusCode = 500;
            $this->data['message'] = $e->getMessage();
        } finally {
            return $this->sendResponse($this->data, $statusCode);
        }
    }
}
