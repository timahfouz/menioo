<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class APIController extends Controller
{
    public $data;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (Auth::guard('diner')->user()) {
            }
            return $next($request);
        });

    }

    public function sendResponse($response, $statusCode)
    {
        return response()->json($response, $statusCode);
    }
    public function me(Request $response)
    {
        $user = Auth::guard('api')->user() ?? Auth::guard('venue')->user();
        return response()->json(['code' => 200, 'data' => $user],200);
    }


}
