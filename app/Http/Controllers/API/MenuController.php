<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\Diner\MenuService;
use App\Http\Services\Diner\ItemService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
Use URL;

class MenuController extends APIController
{
    private $menuObj;
    private $itemObj;

    public function __construct(MenuService $menuObj, ItemService $itemObj)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            $this->customer = Auth::guard('venue')->user();
            //$this->customerId = $this->customer ? $this->customer->id : $this->user->customer_id;
            return $next($request);
        });

        $this->menuObj = $menuObj;
        $this->itemObj = $itemObj;
    }


    public function index(Request $request)
    {
        try {
            $locale = $request->locale;
            $statusCode = 200;

            $this->data['message'] = "data received";
            $this->data['menus'] = [];
            //$menus = $this->menuObj->getAll([['parent_id', '=', 0], 'customer_id' => $this->customerId, 'is_active' => 1]);
            $menus = $this->menuObj->getAll([['parent_id', '=', 0], 'customer_id' => $request->customer_id, 'is_active' => 1]);
            if (count($menus)) {
                foreach ($menus as $menu) {
                    $obj = new \stdClass();
                    $obj->id = $menu->id;
                    $obj->name = $menu->translate($locale,true)->name;
                    $obj->description = $menu->translate($locale,true)->description;
                    $obj->display_as = $menu->display_as;
                    $obj->num_of_rows = $menu->num_of_rows;
                    $obj->grid_title_position = $menu->grid_title_position;
                    $obj->display_similar_items = $menu->display_similar_items;
                    $obj->mark_as_new = $menu->mark_as_new;
                    $obj->mark_as_signature = $menu->mark_as_signature;
                    //$obj->created_at = $menu->created_at->diffForHumans();
                    $obj->menu_image = base64_encode(file_get_contents($menu->FullThumbnailImagePath('335X165')));
                    $this->data['menus'][] = $obj;
                }
            }

        } catch (\Exception $e) {
            $statusCode = 500;
            $this->data['message'] = $e->getMessage();
        } finally {
            return Controller::jsonResponse($statusCode,'Done',$this->data);
            //return $this->sendResponse($this->data, $statusCode);
        }

    }

    public function show(Request $request,$locale,$id)
    {
        try {
            $statusCode = 200;
            $message = 'done.';

            $locale = $request->locale;
            $menu = $this->menuObj->find(['id' => $id, 'is_active' => 1]);
            if(!$menu) {
                $statusCode = 404;
                throw new \Exception('Menu not found');
            }
            $customerID = $menu->customer_id;

            $data = $menu->arrangmentall;
            if(count($data)) {
                foreach($data as $arrangment) {
                    $model = '';
                    $id = $arrangment->arrangmentable_id;
                    $type = (strpos($arrangment->arrangmentable_type, 'Menu') !== false) ? 'Menu' : 'Item';


                    switch($type) {
                        case 'Menu': {
                            $model = $this->menuObj->find(['id'=>$id]);
                            $arrangment->display_as = $arrangment->arrangmentable->display_as;
                            $arrangment->num_of_rows = $arrangment->arrangmentable->num_of_rows;
                            $arrangment->grid_title_position = $arrangment->arrangmentable->grid_title_position;
                            $arrangment->mark_as_new = $arrangment->arrangmentable->mark_as_new;
                            $arrangment->mark_as_new = $arrangment->arrangmentable->mark_as_new;
                            $arrangment->mark_as_signature = $arrangment->arrangmentable->mark_as_signature;
                            //$arrangment->display_similar_items = $arrangment->arrangmentable->display_similar_items;

                            $arrangment->sub_menus = [];
                            if (count($model->SubMenus)) {
                                $subMenu_arr = [];
                                foreach ($model->SubMenus as $subMenu) {
                                    if($subMenu->is_active) {
                                        $subMenu_obj = new \stdClass();
                                        $subMenu_obj->id = $subMenu->id;
                                        $subMenu_obj->is_last_child = $subMenu->is_last_child;
                                        $subMenu_obj->is_active = $subMenu->is_active;
                                        $subMenu_obj->name = $subMenu->name;
                                        $subMenu_obj->description = $subMenu->description;
                                        $subMenu_obj->mark_as_new = $subMenu->mark_as_new;
                                        $subMenu_obj->mark_as_signature = $subMenu->mark_as_signature;
                                        $subMenu_obj->menu_image = base64_encode(file_get_contents($subMenu->FullThumbnailImagePath('335X165')));
                                        $subMenu_arr[] = $subMenu_obj;
                                    }
                                }
                                $arrangment->sub_menus = $subMenu_arr;
                            }
                        } break;
                        case 'Item': {
                            $model = $this->itemObj->find(['id'=>$id]);
                            if($model) {
                                //$obj = new \stdClass();
                                $arrangment->id = $model->id;
                                $arrangment->name = $model->translateOrNew($locale, true)->name;
                                $arrangment->description = $model->translateOrNew($locale, true)->description;
                                $arrangment->portion = $model->portion;
                                $arrangment->preparation_time = $model->preparation_time;
                                $arrangment->calories = $model->calories;
                                $arrangment->notes = $model->translateOrNew($locale, true)->notes;
                                $arrangment->campaigns = $model->campaigns;
                                $arrangment->item_image = base64_encode(file_get_contents($model->FullThumbnailImagePath('335X165')));

                                $arrangment->prices = $model->priceList ? $model->priceList : [];
                                $arrangment->ingredients_warnings = $model->ingredientWarnings ? $model->ingredientWarnings : [];
                                //$obj->side_dishes = $model->sideDishes ? $model->sideDishes : [];
                                //$obj->options = $model->modelOptions ? $model->modelOptions : [];

                                if (count($model->sideDishes)) {
                                    foreach ($model->sideDishes as $sideDish) {
                                        $sideDishObj = new \stdClass();
                                        $sideDishObj->id = $sideDish->side_dish_id;
                                        if($sideDish->side_dishe) {
                                          $sideDishObj->name = $sideDish->side_dishe->name;
                                          $sideDishObj->sideDish_image = base64_encode(file_get_contents($sideDish->side_dishe->FullThumbnailImagePath('335X165')));
                                        }
                                        else {
                                          $sideDishObj->sideDish_image = "";
                                          $sideDishObj->name = "";
                                        }
                                        $sideDishObj->price = $sideDish->price;

                                        $sideDishes_arr[] = $sideDishObj;
                                    }
                                    $arrangment->side_dishes = $sideDishes_arr;
                                }

                                if (count($model->itemOptions)) {
                                    $itemOptions = $model->itemOptions;
                                    $itemOptions->map(function($io){
                                        $io->name = $io->option->name ?? "";
                                        $io->type = $io->type ?? "";
                                        $io->values = $io->optionValues;
                                        $io->makeHidden(['option','item_id','option_id','created_at','updated_at','values']);
                                    });
                                    $arrangment->options = $itemOptions;
                                } else {
                                  $arrangment->options = [];
                                }

                                if(count($model->recommendeditems)) {
                                    $suggested = [];
                                    foreach($model->recommendeditems as $recommended) {
                                        $recomObj = new \stdClass();
                                        $recomObj->id = $recommended->id;
                                        $recomObj->name = $recommended->translateOrNew($locale, true)->name;
                                        $recomObj->description = $recommended->translateOrNew($locale, true)->description;
                                        $recomObj->portion = $recommended->portion;
                                        $recomObj->preparation_time = $recommended->preparation_time;
                                        $recomObj->calories = $recommended->calories;
                                        $recomObj->notes = $recommended->translateOrNew($locale, true)->notes;
                                        $recomObj->campaigns = $recommended->campaigns;
                                        $recomObj->item_image = base64_encode(file_get_contents($recommended->FullThumbnailImagePath('335X165')));
                                        $recomObj->prices = $recommended->priceList ? $recommended->priceList : [];

                                        $suggested[] = $recomObj;
                                    }
                                    $arrangment->recommended = $suggested;
                                }
                            }
                        } break;
                    }

                    $arrangment->id = $id;
                    $arrangment->name = $model->translate($locale, true)->name;
                    $arrangment->description = $model->translate($locale, true)->description;
                    $arrangment->type = $type;
                    $arrangment->image = base64_encode(file_get_contents($arrangment->arrangmentable->FullThumbnailImagePath('300X300')));
                    $arrangment->is_active = $arrangment->arrangmentable->is_active;
                    $arrangment->is_last_child = $arrangment->arrangmentable->is_last_child ? true : false;
                    $arrangment->makeHidden(['arrangmentable','arrangmentable_type','arrangmentable_id','created_at','updated_at','deleted_at']);
                    if($arrangment->type == 'Item')
                        $arrangment->makeHidden(['is_last_child']);
                
                }
            }
            $this->data = $data;
        } catch (\Exception $e) {
            //$statusCode = $statusCode != 404 ? $statusCode : 500;
            $message = $e->getMessage().', line '.$e->getLine();
        } finally {
            return Controller::jsonResponse($statusCode,$message,$this->data);
            //return $this->sendResponse($this->data, $statusCode);
        }
    }
}
