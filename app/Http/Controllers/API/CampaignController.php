<?php

namespace App\Http\Controllers\API;

use App\Http\Services\Diner\CampaignService;
use App\Http\Services\Admin\CustomerServices;
use App\Http\Services\Common\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use URL;

class CampaignController extends APIController
{
    private $campaignObj;
    private $customerObj;
    private $helperObj;

    public function __construct(
        CampaignService $campaignObj,
        CustomerServices $customerObj,
        Helper $helperObj
    )
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('api')->user();
            return $next($request);
        });

        $this->campaignObj = $campaignObj;
        $this->customerObj = $customerObj;
        $this->helperObj = $helperObj;
    }

    public function getCampaigns(Request $request)
    {
        try{
            $locale =  $request->locale;

            $statusCode = 200;
            $message = 'Done!';

            $campaigns = $this->campaignObj->getAll(['active'=>1,'customer_id'=>$request->customer_id,['item_id','=',null]]);


            if(!count($campaigns)) {
                $statusCode = 404;
                $message = 'There\'re no campaigns available!';
                throw new \Exception($message);
            } else {
                $campaigns->map(function($item) {
                    $item->image_path = $item->FullImagePath();
                    $item->video_path = $item->FullVideoPath();
                    $item->makeHidden(['image','video','image_id','video_id']);
                });
            }
        }   catch(\Exception $e) {
            $statusCode = $statusCode ? $statusCode : 500;
            $message = $e->getMessage();
        }   finally{
            return parent::jsonResponse($statusCode,$message,$campaigns);
        }

    }

    public function create(Request $request)
    {
        try {
            $statusCode = 201;
            $message = 'Done!';
            if(!$this->user) {
              throw new \Exception('You must loggin as a waiter!');
            }
            $data = [];
            $identifier = $this->feedbackInteractionObj->getIdentifier();
            if(count($request->answers)) {
                $identifier = $this->feedbackInteractionObj->getIdentifier();

                foreach($request->answers as $answer) {
                    $answer['status'] = 1;
                    $answer['customer_user_id'] = $this->user->id;
                    $answer['identifier'] = $identifier;
                    $data[] = $answer;
                }
            }
            $this->feedbackInteractionObj->bulkInsert($data);
            $statusCode = 201;

        } catch(\Exception $e) {
            $message = $e->getMessage().' => '.$e->getLine();
        } finally {
            return parent::jsonResponse($statusCode,$message,$data);
        }
    }

}
