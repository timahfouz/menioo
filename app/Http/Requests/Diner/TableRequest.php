<?php

namespace App\Http\Requests\Diner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'customer_id' => 'required|exists:customers,id',
                        'number' => 'required|unique:tables,number,customer_id',
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        'customer_id' => 'required|exists:customers,id',
                        'number' => 'required|unique:tables,number,customer_id',
                    ];
                }
            default:
                break;
        }
    }
}
