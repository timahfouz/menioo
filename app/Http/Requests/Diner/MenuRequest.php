<?php

namespace App\Http\Requests\Diner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => 'required|unique:menus,name,null,id,parent_id,0,customer_id,' . Auth::guard('diner')->user()->id,
                        'description' => 'required',
                        'display_as' => 'required',
                        'num_of_rows' => 'required_if:display_as,grid',
                        'grid_title_position' => 'required_if:display_as,grid',
                        'file' => 'mimes:jpeg,jpg,png',
                        'video' => 'mimes:mp4'
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        'name' => 'required|unique:menus,name,' . $this->segment(3) . ',id,parent_id,0,customer_id,' . Auth::guard('diner')->user()->id,
                        'display_as' => 'required',
                        'num_of_rows' => 'required_if:display_as,grid',
                        'grid_title_position' => 'required_if:display_as,grid',
                        'file' => 'mimes:jpeg,jpg,png',
                        'video' => 'mimes:mp4'
                    ];
                }
            default:
                break;
        }
    }
}
