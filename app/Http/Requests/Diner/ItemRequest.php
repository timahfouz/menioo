<?php

namespace App\Http\Requests\Diner;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        //'name' => 'required|unique:items',
                        'name' => 'required|unique:items,name,null,id,menu_id,' . $this->segment(3),
                        //'description' => 'required',
                        //'portion' => 'required',
                        'preparation_time' => 'required',
                        //'calories' => 'required',
                        //'prices' => 'required|array|min:1',
                        'file' => 'mimes:jpeg,jpg,png',
                        'video' => 'mimes:mp4'
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    return [
                        //'name' => 'required|unique:items',
                        'name' => 'required|unique:items,name,' . $this->segment(5) . ',id,menu_id,' . $this->segment(3),
                        //'description' => 'required',
                        //'portion' => 'required',
                        'preparation_time' => 'required',
                        //'calories' => 'required',
                        //'prices' => 'required|array|min:1',
                        'file' => 'mimes:jpeg,jpg,png',
                        'video' => 'mimes:mp4'
                    ];
                }
            default:
                break;
        }
    }
}
