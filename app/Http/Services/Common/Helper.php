<?php namespace App\Http\Services\Common;


use Intervention\Image\Facades\Image;
use phpDocumentor\Reflection\Types\Self_;

class Helper
{
    const UPLOAD_DIR = 'media/';
    const THUMBNAIL_DIR = 'thumbnail/';
    const SIZES = [[100, 100], [200, 200], [300, 300], [500, 500], [517, 255],[335,165]];

    private $mediaObj;

    public function __construct(MediaService $mediaObj)
    {
        $this->mediaObj = $mediaObj;
    }

    public function upload($file, $data = [], $method = false, $old_file = false)
    {
        try {

            if ($method == 'edit' && $old_file) {
                $path = public_path(self::UPLOAD_DIR . $old_file->media_path);

                if (file_exists($path))
                    unlink($path);

                foreach (self::SIZES as $size) {
                    $path = public_path(self::THUMBNAIL_DIR . str_replace('.' . $old_file->media_ext, '-' . $size[0] . 'X' . $size[1] . '.' . $old_file->media_ext, $old_file->media_path));
                    //dd($path);
                    if (file_exists($path))
                        unlink($path);
                }

            }
            $file_name = substr(md5(time()), 0, 15);

            $extension = $file->getClientOriginalExtension();
            $fileFullName = $file->getClientOriginalName();
            $mimeType = $file->getMimeType();
            $media_title = count($data) && isset($data['title']) ? $data['title'] : explode('.' . $extension, $fileFullName)[0];

            $fileName = $file_name . "." . $extension;


            /*
             * THUMBNAILING IMAGE
             */
            if (substr($file->getMimeType(), 0, 5) == 'image') {
                foreach (self::SIZES as $size) {
                    $img = Image::make($file->getRealPath());
                    $img->resize($size[0], $size[1], function ($constraint) {
                        $constraint->aspectRatio();
                    })
                        //->insert('public/watermark.png')
                        ->save(self::THUMBNAIL_DIR . '/' . $file_name . "-" . $size[0] . 'X' . $size[1] . "." . $extension);
                }
            }


            $destinationPath = public_path(self::UPLOAD_DIR);
            $file->move($destinationPath, $fileName);

            return $this->mediaObj->insert(['media_path' => $fileName, 'media_title' => $media_title, 'media_type' => $mimeType,
                'media_ext' => $extension, 'customer_id' => $data['customer_id']]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function parseDataFromOutSource($url, $request = 'GET', $data = [])
    {

        $response = [];
        if ($request == 'POST') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0";
            curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json;charset=UTF-8']);
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_ENCODING, "compress, gzip");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
        } else if ($request == 'GET') {
            $curlSession = curl_init();
            curl_setopt($curlSession, CURLOPT_URL, $url);
            curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
            $response = json_decode(curl_exec($curlSession));
            curl_close($curlSession);
            //$response = file_get_contents($url);

        }
        return $response;
    }
}