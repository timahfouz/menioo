<?php namespace App\Http\Services\Admin;

use Illuminate\Support\Facades\Auth;


class AdminLoginService
{

    private $auth;
    public function __construct()
    {
        $this->auth = Auth::guard('admin');
    }

    public function login($credentials, $remember = 0)
    {
        return $this->auth->attempt($credentials, $remember);
    }

    public function logout()
    {
        return $this->auth->logout();
    }
}
