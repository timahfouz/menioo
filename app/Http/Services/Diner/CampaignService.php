<?php

namespace App\Http\Services\Diner;

use App\Http\Models\Campaign;


class CampaignService
{

    private $model;
    public function __construct(Campaign $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;

        return $per_page ? $items->where($conditions)->paginate($per_page) : $items->where($conditions)->get();
    }

    public function getAllinTime($conditions, $dates){
        $items = $this->model;

        return $items->where($conditions)->whereBetween('created_at', $dates)->get();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
