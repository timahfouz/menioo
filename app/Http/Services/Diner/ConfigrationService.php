<?php

namespace App\Http\Services\Diner;

use App\Http\Models\Configration;


class ConfigrationService
{

    private $model;
    public function __construct(Configration $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;

        return $per_page ? $items->where($conditions)->orderBy('order','desc')->paginate($per_page) : $items->where($conditions)->get();
    }


    public function insert($data)
    {
        return $this->model->firstOrCreate($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }
    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}