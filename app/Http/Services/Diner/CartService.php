<?php namespace App\Http\Services\Diner;

use App\Http\Models\Cart;
use App\Http\Models\CartItemOption;
use App\Http\Models\ItemOption;


class CartService
{

    private $model;
    private $cartItemObj;
    private $itemOptionObj;
    public function __construct(
      Cart $model,
      CartItemOption $cartItemObj,
      ItemOption $itemOptionObj
    )
    {
        $this->model = $model;
        $this->cartItemObj = $cartItemObj;
        $this->itemOptionObj = $itemOptionObj;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;
        return $per_page ? $items->where($conditions)->paginate($per_page) : $items->where($conditions)->get();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function getLastHandled($conditions)
    {
        return $this->model->where($conditions)->orderBy('created_at','DESC')->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
