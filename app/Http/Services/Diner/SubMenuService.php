<?php namespace App\Http\Services\Diner;

use App\Http\Models\Menu;
use App\Http\Models\Arrangment;


class SubMenuService
{

    private $model;
    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        $obj = $this->model->create($data);
        Arrangment::create(
            [
                'menu_id' => $obj->parent_id,
                'customer_id' => $obj->customer_id,
                'order' => 0,
                'arrangmentable_type' => 'App\Http\Models\Menu',
                'arrangmentable_id' => $obj->id
            ]
        );
        return $obj;
    }


    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions)
    {
        return $this->model->where($conditions)->first();
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->with(['MenuImage'])->where($conditions)->paginate($per_page) : $this->model->with(['MenuImage'])->where($conditions)->get();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function getTotalCount($conditions = [])
    {
        return count($conditions) ? $this->model->where($conditions)->count() : $this->model->count();
    }
}
