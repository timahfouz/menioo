<?php namespace App\Http\Services\Diner;

use App\Http\Models\OptionValue;


class OptionValueService
{

    private $model;
    public function __construct(OptionValue $model)
    {
        $this->model = $model;
    }


    public function getAll($conditions=[], $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->paginate($per_page) : $this->model->where($conditions)->get();
    }

    public function find($conditions)
    {
        return $this->model->where($conditions)->first();
    }

    public function insert($values, $option, $prices=[])
    {
        if(count($prices)){
            foreach ($values as $key => $value) {
                $this->model->create([
                    'option_id' => $option->id,
                    'value' => $value,
                    'price' => $prices[$key]
                ]);
            }
        }
        else{
            foreach ($values as $key => $value) {
                $this->model->create([
                    'option_id' => $option->id,
                    'value' => $value
                ]);
            }
        }
        return "Done";
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
