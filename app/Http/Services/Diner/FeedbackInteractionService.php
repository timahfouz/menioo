<?php

namespace App\Http\Services\Diner;

use App\Http\Models\FeedbackInteraction;
use DB;
use Carbon\Carbon;

class FeedbackInteractionService
{

    private $model;
    public function __construct(FeedbackInteraction $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false, $dates = [], $forms=[])
    {
        $result = [];
        $identifiers = $this->model->where($conditions)->whereIn('form_id',$forms);
        
        $identifiers = $per_page ? $identifiers->paginate($per_page)->pluck('identifier')->toArray() : $identifiers->pluck('identifier')->toArray() ;
        

        $identifiers = array_unique($identifiers);
        if(count($identifiers)) {
            foreach($identifiers as $identifier) {
                $data = [];
                $items = count($with) ? $this->model->with($with) : $this->model;
                $items = count($dates) ? $items->where('identifier',$identifier)->whereBetween('created_at',$dates)->get() : $items->where('identifier',$identifier)->get();

                $items->map(function($item){
                    $data['form_name'] = $item->form ? $item->form->form_name : 'Form Deleted';
                    $data['form_header'] = $item->form ? $item->form->form_header : 'Form Deleted';
                    $data['query'] = $item->question ? $item->question->value : 'Form Questions Deleted';
                    $item->makeHidden(['customer','form']);
                });
                if(count($items)) {
                    $data['id'] = $items[0]->form ? $items[0]->form->id : 0;
                    $data['form_name'] = $items[0]->form ? $items[0]->form->form_name : 'Form Deleted';
                    $data['form_header'] = $items[0]->form ? $items[0]->form->form_header : 'Form Deleted';
                    $data['active'] = $items[0]->form ? $items[0]->form->active : 0;
                    $data['identifier'] = $items[0]->identifier;
                    $data['status'] = $items[0]->status;
                    $data['date'] = date('d/m/Y',strtotime($items[0]->created_at));
                    $data['time'] = date('h:m a',strtotime($items[0]->created_at));
                }
                $data['interactions'] = $items;
                $result[] = (object)$data;
            }
        }
        return $result;
    }

    public function getAllInTime($conditions, $dates){
        $result = [];
        $identifiers = $this->model->where($conditions);
        
        $identifiers = $identifiers->pluck('identifier')->toArray() ;
        

        $identifiers = array_unique($identifiers);
        if(count($identifiers)) {
            foreach($identifiers as $identifier) {
                $data = [];
                $items =  $this->model;
                $items = count($dates) ? $items->where('identifier',$identifier)->whereBetween('created_at',$dates)->get() : $items->where('identifier',$identifier)->get();

                $items->map(function($item){
                    $data['form_name'] = $item->form ? $item->form->form_name : 'Form Deleted';
                    $data['form_header'] = $item->form ? $item->form->form_header : 'Form Deleted';
                    $data['query'] = $item->question ? $item->question->value : 'Form Questions Deleted';
                    $item->makeHidden(['customer','form']);
                });
                if(count($items)) {
                    $data['id'] = $items[0]->form ? $items[0]->form->id : 0;
                    $data['form_name'] = $items[0]->form ? $items[0]->form->form_name : 'Form Deleted';
                    $data['form_header'] = $items[0]->form ? $items[0]->form->form_header : 'Form Deleted';
                    $data['active'] = $items[0]->form ? $items[0]->form->active : 0;
                    $data['identifier'] = $items[0]->identifier;
                    $data['status'] = $items[0]->status;
                    $data['date'] = date('d/m/Y',strtotime($items[0]->created_at));
                    $data['time'] = date('h:m a',strtotime($items[0]->created_at));
                }
                $data['interactions'] = $items;
                if(count($data['interactions']))
                    $result[] = (object)$data;
            }
        }
        return $result;

    }


    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function bulkInsert($data)
    {
        return $this->model->insert($data);
    }

    public function getIdentifier()
    {
        $last = $this->model->orderByDesc('identifier')->first();
        return $last ? $last->identifier + 1 : 1;
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function findall($ids){
        return $this->model->whereIn('identifier', $ids);
    }


    public function recentfeedback($conditions, $dates = []){
        if(! count($dates))
            $dates = [Carbon::now(), Carbon::now()->addDay()];
        return $this->model->where($conditions)->whereBetween('created_at', $dates)->get();
        
    }

}
