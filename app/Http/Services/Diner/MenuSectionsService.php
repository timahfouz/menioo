<?php namespace App\Http\Services\Diner;

use App\Http\Models\MenuSections;


class MenuSectionsService
{

    private $model;
    public function __construct(MenuSections $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }


    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function updateOrCreate($conditions, $data)
    {
        return $this->model->where($conditions)->updateOrCreate($data);
    }

    public function find($conditions)
    {
        return $this->model->where($conditions)->orderBy('id', 'DESC')->first();
    }

    public function getLastOne($conditions = [])
    {
        return count($conditions) ? $this->model->where($conditions)->orderBy('id', 'DESC')->first() : $this->model->orderBy('id', 'DESC')->first();
    }

    public function getAll($conditions, $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->paginate($per_page) : $this->model->where($conditions)->get();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function getTotalCount($conditions = [])
    {
        return count($conditions) ? $this->model->where($conditions)->count() : $this->model->count();
    }
}
