<?php

namespace App\Http\Services\Diner;

use App\Http\Models\FeedbackQuestion;


class FeedbackQuestionService
{

    private $model;
    public function __construct(FeedbackQuestion $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false)
    {
        $items = count($with) ? $this->model->with($with) : $this->model;

        return $per_page ? $items->where($conditions)->paginate($per_page) : $items->where($conditions)->get();
    }


    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function bulkInsert($data)
    {
        return $this->model->insert($data);
    }

    public function update($conditions, $data)
    {
        $question = $this->find($conditions);
        $question->{'value:en'} = $data['value'];
        $question->save();
        return $this->model->where($conditions)->update(['type' => $data['type']]);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    public function deleteQuestion($ids, $feedback_id){
        return $this->model->whereNotIn('id', $ids)->where('feedback_id', $feedback_id)->delete();
    }

}
