<?php namespace App\Http\Services\Diner;

use App\Http\Models\CartItem;


class CartItemService
{

    private $model;
    public function __construct(CartItem $model)
    {
        $this->model = $model;
    }

    public function getAll($conditions, $with=[], $per_page = false, $whereIn = [])
    {
        $items = count($with) ? $this->model->with($with)->where($conditions) : $this->model->where($conditions);
        $items = (is_array($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $items->whereIn($whereIn['key'], $whereIn['values']) : $items;
        return $per_page ? $items->paginate($per_page) : $items->get();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function bulkInsert($data)
    {
        return $this->model->insert($data);
    }
    
    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
