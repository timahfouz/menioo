<?php namespace App\Http\Services\Diner;

use App\Http\Models\Option;
use App\Http\Services\Diner\OptionValueService;

class OptionService
{

    private $model;
    private $optionvalue;
    public function __construct(Option $model, OptionValueService $optionvalue)
    {
        $this->model = $model;
        $this->optionvalue = $optionvalue;
    }


    public function getAll($conditions=[], $per_page = false)
    {
        return $per_page ? $this->model->where($conditions)->paginate($per_page) : $this->model->where($conditions)->get();
    }

    public function find($conditions)
    {
        return $this->model->with('values')->where($conditions)->first();
    }

    public function insert($data)
    {
        $option = $this->model->create([
            'name' => $data['name'],
            'type' => $data['type'],
            'customer_id' => $data['customer_id'],

        ]);
        if(isset($data['prices']))
            $this->optionvalue->insert($data['values'], $option, $data['prices']);
        else
            $this->optionvalue->insert($data['values'], $option);

        return $option;

    }

    public function update($conditions, $data)
    {
        $option =  $this->model->where($conditions)->update([
            'name' => $data['name'],
            'type' => $data['type'],
        ]);
        $option = $this->find($conditions);
        $this->optionvalue->delete(['option_id' => $option->id]);
        if(isset($data['prices']))
            $this->optionvalue->insert($data['values'], $option, $data['prices']);
        else
            $this->optionvalue->insert($data['values'], $option);

        return $option;
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
