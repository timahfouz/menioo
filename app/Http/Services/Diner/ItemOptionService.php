<?php namespace App\Http\Services\Diner;

use App\Http\Models\ItemOption;


class ItemOptionService
{

    private $model;
    public function __construct(ItemOption $model)
    {
        $this->model = $model;
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function bulkInsert($data)
    {
        return $this->model->insert($data);
    }

    public function update($conditions, $data)
    {
        return $this->model->where($conditions)->update($data);
    }

    public function getAll($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->get();
        else
            return $this->model->where($conditions)->get();
    }

    public function find($conditions,$with=[])
    {
        if(count($with))
            return $this->model->with($with)->where($conditions)->first();
        else
            return $this->model->where($conditions)->first();
    }

    public function delete($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

}
