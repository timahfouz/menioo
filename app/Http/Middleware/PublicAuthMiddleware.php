<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class PublicAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('api')->check() || Auth::guard('venue')->check())
          return $next($request);
        return response()->json([
          'code' => 403,
          'message' => 'not authenticated!',
          'data' => []
        ],403);
    }
}
