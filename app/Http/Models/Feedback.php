<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;


class Feedback extends Model
{
    protected $guarded = [];
    // public $translatedAttributes = ['form_name','form_header'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function questions()
    {
        return $this->hasMany(FeedbackQuestion::class, 'feedback_id');
    }


}
