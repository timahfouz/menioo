<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Dimsav\Translatable\Translatable;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use Translatable;

    public $translatedAttributes = ['name','description', 'additional_notes'];
    public $translationModel = 'App\Http\Models\MenuTranslation';

    protected $table = 'menus';

    protected $fillable = [
        'customer_id', 'parent_id', 'name', 'description', 'additional_notes', 'display_as', 'num_of_rows', 'grid_title_position',
        'display_similar_items', 'mark_as_new', 'mark_as_signature', 'additional_notes', 'media_id', 'video_id', 'is_active', 'section_id'
    ];

    // public $translatedAttributes = ['name', 'description', 'additional_notes'];


//    public function translate()
//    {
//        return $this->hasMany(\App\Http\Models\MenuTranslation::class);
//    }

    public function MenuImage()
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    public function MenuVideo()
    {
        return $this->belongsTo(Media::class, 'video_id', 'id');
    }

    public function Customer()
    {
        return $this->belongsTo(Customer::class, 'id', 'customer_id');
    }

    public function SubMenus()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id');
    }
    
    public function items()
    {
        return $this->hasMany(Item::class, 'menu_id', 'id');
    }

    public function Sections()
    {
        return $this->hasMany(MenuSections::class, 'menu_id', 'id');
    }

    public function MenuParent()
    {
        return $this->belongsTo(Menu::class, 'parent_id' , 'id');
    }

    public function FullImagePath()
    {
        return $this->MenuImage ? URL::to('media/' . $this->MenuImage->media_path) : '';
    }

    public function FullThumbnailImagePath($size = '500X500')
    {
        return $this->MenuImage ? URL::to('thumbnail/' . str_replace('.' . $this->MenuImage->media_ext, '-' . $size . '.' . $this->MenuImage->media_ext, $this->MenuImage->media_path)) : '';
    }

    public function FullVideoPath()
    {
        return $this->MenuVideo ? URL::to('media/' . $this->MenuVideo->media_path) : '';
    }

    public function getIsLastChildAttribute()
    {
        return count($this->SubMenus) ? false : true;
    }

    protected $appends = ['is_last_child'];

    public function arrangment()
    {
        return $this->morphOne('App\Http\Models\Arrangment', 'arrangmentable');
    }

    public function arrangmentall(){
        return $this->hasMany('\App\Http\Models\Arrangment', 'menu_id')->with('arrangmentable')->orderBy('order');
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function($menu){
            $menu->translate()->delete();
            $menu->SubMenus->delete();
            $menu->items->delete();
        });
    }

}
