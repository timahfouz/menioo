<?php

namespace App\Http\Models;


use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use URL;

class Customer extends Authenticatable implements JWTSubject
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes, Notifiable;


    protected $table = 'customers';

    protected $fillable = [
        'name', 'password', 'company_name', 'occupation', 'mobile_number', 'email', 'country_id', 'city_id', 'state', 'is_active', 'active_deactivate_date','logo_id','parent_id','description','deleted_at','address','timezone','website','instagram','facebook','twitter','foursquare','waiter_login','status'
    ];

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function plan()
    {
        return $this->belongsTo(Subscription::class, 'plan_id');
    }
    public function Country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo(Country::class, 'city_id', 'id');
    }

    public function Users()
    {
        return $this->hasMany(CustomerUser::class, 'customer_id', 'id');
    }

    public function mainBranch()
    {
        return $this->belongsTo(Customer::class, 'parent_id');
    }

    public function venues()
    {
        return $this->hasMany(Customer::class, 'parent_id');
    }

    public function Menus()
    {
        return $this->hasMany(Menu::class, 'customer_id', 'id');
    }

    public function mainMenus()
    {
        return $this->Menus->where('parent_id',0);
    }

    public function items()
    {
        return Item::whereIn('menu_id',$this->Menus->pluck('id'))->get();
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class, 'customer_id');
    }

    public function devices()
    {
        return $this->hasMany(Device::class, 'customer_id');
    }

    public function logo()
    {
        return $this->belongsTo(Media::class, 'logo_id');
    }

    public function FullLogoPath()
    {
        return $this->logo ? URL::to('media/' . $this->logo->media_path) : '';
    }

    public function FullThumbnailLogoPath($size = '500X500')
    {
        return $this->logo ? URL::to('thumbnail/' . str_replace('.' . $this->logo->media_ext, '-' . $size . '.' . $this->logo->media_ext, $this->logo->media_path)) : '';
    }

    public function sidedishes(){
        return $this->hasMany('App\Http\Models\SideDish', 'customer_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }


}
