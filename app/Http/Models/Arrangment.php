<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Arrangment extends Model
{
    //
    protected $table = 'arrangments';

    protected $fillable = [
        'menu_id',
        'customer_id',
        'order',
        'arrangmentable_type',
        'arrangmentable_id'
    ];

    public function arrangmentable()
    {
        return $this->morphTo();
    }

}
