<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];
    protected $hidden = ['cartItems'];

    protected $table = 'carts';

    public function customer()
    {
        return $this->belongsTo(CustomerUser::class,'customer_id');
    }
    public function items()
    {
        return $this->belongsToMany(Item::class,'cart_items','cart_id','item_id');
    }
    public function cartItems()
    {
        return $this->hasMany(cartItem::class,'cart_id');
    }

    public function itemOptions($itemID)
    {
        return CartItemOption::where(['cart_id' => $this->id, 'item_id' => $itemID])->get();
    }
    public function itemSideDishes($itemID)
    {
        return CartItemSideDish::where(['cart_id' => $this->id, 'item_id' => $itemID])->get();
    }
    /*public function itemOption()
    {
        return $this->belongsToMany(ItemOption::class,'cart_item_options','cart_item_id','item_option_id');
    }
    public function itemSideDishes()
    {
        return $this->belongsToMany(ItemSideDish::class,'cart_item_side_dishes','cart_item_id','item_side_dishe_id');
    }*/

    public static function boot()
    {
        parent::boot();
        static::deleting(function($cart){
            CartItem::where('cart_id',$cart->id)->delete();
            CartItemOption::where('cart_id',$cart->id)->delete();
            CartItemSideDish::where('cart_id',$cart->id)->delete();
        });
    }

}
