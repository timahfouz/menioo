<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CartItemOption extends Model
{
    protected $guarded = [];

    public function cartItem()
    {
        return $this->belongsTo(CartItem::class,'cart_item_id');
    }

    public function itemOption()
    {
        return $this->belongsTo(ItemOption::class,'item_option_id');
    }
}
