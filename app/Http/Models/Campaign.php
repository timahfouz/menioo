<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use URL;

class Campaign extends Model
{
    protected $table = 'campaigns';

    protected $guarded = [];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function image()
    {
        return $this->belongsTo(Media::class, 'image_id');
    }

    public function video()
    {
        return $this->belongsTo(Media::class, 'video_id');
    }

    public function FullImagePath()
    {
        return $this->image ? URL::to('media/' . $this->image->media_path) : '';
    }

    public function FullThumbnailImagePath($size = '500X500')
    {
        return $this->image ? URL::to('thumbnail/' . str_replace('.' . $this->image->media_ext, '-' . $size . '.' . $this->image->media_ext, $this->image->media_path)) : '';
    }

    public function FullVideoPath()
    {
        return $this->video ? URL::to('media/' . $this->video->media_path) : '';
    }

    protected $casts = [
        'automatically_end' => 'bool',
    ];
}
