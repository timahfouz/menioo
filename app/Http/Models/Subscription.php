<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'plans';

    protected $fillable = [
        'name',
        'type',
        'billing_period',
        'plan_type',
        'plan_id',
        'price',
        'tablets_number',
        'features',
        'order',
        'order_option'

    ];

    public function subscribers()
    {
        return $this->hasMany(Customer::class, 'plan_id');
    }
}
