<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $guarded = [];

    public function cart()
    {
        return $this->belongsTo(Cart::class,'cart_id');
    }
    public function customerUser()
    {
        return $this->belongsTo(CustomerUser::class,'customer_user_id');
    }

    
}
