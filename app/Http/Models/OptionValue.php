<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class OptionValue extends Model
{
    //
    protected $fillable = [
        'option_id', 'value', 'price'
    ];
}
