<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $table = 'media';

    protected $fillable = [
        'media_path', 'media_title', 'media_type', 'media_ext', 'customer_id'
    ];
}
