<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSections extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $table = 'menu_sections';

    protected $fillable = [
        'menu_id', 'title'];

    public function Menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }

    public function SubMenus()
    {
        return $this->hasMany(Menu::class, 'section_id', 'id');
    }

}
