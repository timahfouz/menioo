<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MenuTranslation extends Model
{
    //
    public $fillable = ['menu_id', 'locale', 'name', 'description', 'additional_notes'];

}
