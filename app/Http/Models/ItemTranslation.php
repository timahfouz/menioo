<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ItemTranslation extends Model
{
    //
    public $fillable = ['item_id', 'locale', 'name', 'description', 'notes'];

}
