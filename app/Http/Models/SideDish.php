<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use URL;

class SideDish extends Model
{
    protected $table = 'side_dishes';

    protected $guarded = [];

    public function image()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function FullImagePath()
    {
        return $this->image ? URL::to('media/' . $this->image->media_path) : '';
    }
    
    public function FullThumbnailImagePath($size = '500X500')
    {
        return $this->image ? URL::to('thumbnail/' . str_replace('.' . $this->image->media_ext, '-' . $size . '.' . $this->image->media_ext, $this->image->media_path)) : '';
    }

}
