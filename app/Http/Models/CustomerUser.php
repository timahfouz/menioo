<?php

namespace App\Http\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerUser extends Authenticatable implements JWTSubject
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'customer_users';

    protected $guard = 'customer';

    protected $fillable = [
        'name', 'email', 'password', 'customer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function mainBranch()
    {
        $customer = $this->Customer;
        return $customer->parent_id != 0 ? $customer->mainBranch : $customer;
    }

    public function cart()
    {
        return $this->hasMany(Cart::class, 'customer_id');
    }

    public function siblinsIDs()
    {
        $parent = $this->Customer;
        return $parent->Users->pluck('id')->toArray();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

}
