<?php

use Illuminate\Http\Request;

use App\Http\Services\Diner\CartService;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API', 'as' => 'api.', 'prefix' => '{locale}'], function () {
    //Route::post('login', ['as' => 'login', 'uses' => 'DinerLoginController@login']);
    Route::post('login', ['as' => 'login', 'uses' => 'PublicController@login']);
    Route::get('conf', ['as' => 'conf', 'uses' => 'PublicController@getCustomerConfig']);

    Route::post('public-login', ['as' => 'login', 'uses' => 'PublicController@venueLogin']);


    Route::group(['middleware' => 'authed'],function(){
        Route::get('me', ['as' => 'me', 'uses' => 'APIController@me']);

        Route::get('menus', ['as' => 'menu.index', 'uses' => 'MenuController@index']);
        Route::get('menu/{id}/show', ['as' => 'menu.show', 'uses' => 'MenuController@show']);

        Route::group(['prefix' => 'items'],function(){
            Route::get('list/{menu_id}', ['as' => 'item.index', 'uses' => 'ItemController@index']);
            Route::get('show/{id}', ['as' => 'item.show', 'uses' => 'ItemController@show']);
            Route::post('create/{menu_id}', ['as' => 'item.index', 'uses' => 'ItemController@store']);
        });

        Route::group(['prefix' => 'cart'],function(){
            Route::get('list', ['as' => 'cart.index', 'uses' => 'CartController@index']);
            Route::post('add', ['as' => 'cart.add', 'uses' => 'CartController@createOrEdit']);
            Route::post('edit', ['as' => 'cart.edit', 'uses' => 'CartController@createOrEdit']);
            //Route::post('submit', ['as' => 'cart.submit', 'uses' => 'CartController@submit']);
            Route::post('delete', ['as' => 'cart.delete', 'uses' => 'CartController@delete']);
        });

        Route::group(['prefix' => 'order'],function(){
            Route::post('submit', ['as' => 'order.submit', 'uses' => 'OrderController@submitOrder']);
            Route::get('opened', ['as' => 'order.opened', 'uses' => 'OrderController@openedOrders']);
            Route::get('close/{id}', ['as' => 'order.close', 'uses' => 'OrderController@closeOrder']);
        });

        Route::group(['prefix' => 'feedback'],function(){
            Route::get('get-form', ['as' => 'feedback.get-form', 'uses' => 'FeedbackInteractionController@getForm']);
            Route::post('send-answers', ['as' => 'feedback.create', 'uses' => 'FeedbackInteractionController@create']);
        });

        Route::group(['prefix' => 'campaign'],function(){
            Route::get('get-campaigns', ['as' => 'campaign.get-campaigns', 'uses' => 'CampaignController@getCampaigns']);
        });

        Route::group(['prefix' => 'tables'],function(){
            Route::get('get-all', ['as' => 'table.get-all', 'uses' => 'TableController@index']);
        });


    });
});
