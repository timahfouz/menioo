@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>Number</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if($tables)
                @foreach($tables as $table)
                    <tr class="viewPaln">
                        <td>{{$table->number}}</td>
                        <td>{{$table->description}}</td>

                        <td>
                            <button class="btn btn-danger edit-table" data-href="{{$table->id}}">EDIT</button>
                            <button class="btn btn-secondary"  data-toggle="modal" data-target="#delAlert_{{$table->id}}" class="">DELETE</button>
                        </td>
                    </tr>
                    <!-- Delete Alert -->
                    <div class="modal fade" id="delAlert_{{$table->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            {{Form::open(['route'=>['diner.tables.destroy' , $table->id ] , 'method'=>'delete'])}}
                            <div class="modal-content text-center">
                                <div class="modal-body">
                                    <h5 class="text-center">ARE YOU SURE?</h5>
                                    <p class="m-0">Are you sure you want to delete table {{$table->number}}?</p>
                                    <small class="text-danger">This cannot be undone</small>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                                    <button type="submit" class="dark-btn">DELETE TABLE</button>
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                @endforeach
            @endif
            </tbody>
        </table>

        <nav>
            <div class="float-right pagination-filter">
                <div>
                    <label>Showing</label>
                    <select class="form-control">
                        <option>10 items per page</option>
                    </select>
                </div>
            </div>
            {{$tables->links()}}
            {{--<ul class="pagination justify-content-center">--}}
            {{--<li class="page-item disabled">--}}
            {{--<a class="page-link" href="#" tabindex="-1"><i--}}
            {{--class="fas fa-angle-left"></i></a>--}}
            {{--</li>--}}
            {{--<li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
            {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
            {{--<li class="page-item">--}}
            {{--<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
            {{--</li>--}}
            {{--</ul>--}}

        </nav>
    </div>
@stop

@section('scripts')
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>

    <script>

        $('.edit-table').on('click',function(){
            id = $(this).attr('data-href');
            window.open('{{URL::to('diner/tables')}}/' + id + '/edit?venue_id={{$venue_id}}' , '_self');
        });

        $('#keep-on .dropdown-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        $(".dismissDD").on('click', function () {
            $(".dropdown-toggle").dropdown("hide");
        });

        $(".select2").select2();




        $('#sort_btn').click(function () {
            var sort_by = $('.sorting-buttons.selected').attr('id');
            if (sort_by) {
                url = updateQueryStringParameter(window.location.href, 'sort_by', sort_by);
                window.location = url;
            }
        });



        $('.sorting-buttons').click(function () {
            $('.sorting-buttons').each(function () {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');
        })
    </script>


@stop
