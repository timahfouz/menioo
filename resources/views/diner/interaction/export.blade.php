
<table class="table table-hover table-borderless" style="margin-top: 50px;">
    <thead>
    <tr>
        <th>Form</th>
        <th>Status</th>
        <th>Sent at</th>
        <th></th>
            @for($i = 1; $i <= 15; $i++)
                <th>Q{{$i}}</th>
                <th>Type{{$i}}</th>
                <th>A{{$i}}</th>
            @endfor
    </tr>
    </thead>
    <tbody>
    @if(count($interactions))
        @foreach($interactions as $interaction)
            <tr class="viewPaln">
                <td>{{$interaction->form}}</td>
                <td>{{$interaction->status}}</td>
                <td>{{$interaction->sent_at}}</td>
                <td></td>
                @if(count($interaction->fqa))
                    @foreach($interaction->fqa as $index => $item)
                        <td>{{$item->question->value}}</td>
                        <td>{{$item->question->type}}</td>
                        <td>{{$item->answer}}</td>
                    @endforeach
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
