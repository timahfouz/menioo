@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/waiters/'.$current_customer->id) ,'method'=>'PUT'))}}

    <div class="row">
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">Full Name</label>
            <input type="text" data-validation="required" name="name" value="{{old('name',$current_customer->name)}}" id="first-name"
                    class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="email">Email</label>
            <input type="email" data-validation="required email"  name="email" value="{{old('email',$current_customer->email)}}" id="email" class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="password">Password</label>
            <input id="password" name="password" type="password"   class="form-control">

        </div>

        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="password-confirm" >Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
            
        </div>
    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">SAVE WAITER</button>
        
        <button type="button" data-toggle="modal" data-target="#delAlert"
                class="plain-btn text-danger"><i class="far fa-trash-alt"></i> DELETE
        </button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')

    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {{Form::open(['route'=>['diner.waiters.destroy' , $current_customer->id ] , 'method'=>'delete'])}}

            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p class="m-0">Are you sure you want to delete {{$current_customer->name}}?</p>
                    <small class="text-danger">This cannot be undone</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="dark-btn">DELETE WAITER</button>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $(".select2").select2();


    </script>


@stop