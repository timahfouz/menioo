@extends('diner.layouts.master')

@section('content')

    <div class="container">
        <input type="hidden" name="current_menu" id="current_menu" value="{{ $current_menu->id }}">

        <div class="row row-title">
            <div class="col-md-12 text-left">
            <i class="fas fa-chevron-left"></i>&nbsp; Menus &nbsp; &nbsp; <i class="fas fa-chevron-left"></i>&nbsp; {{ $current_menu->name }}
            </div>
            <div class="col-md-3" style="padding-top: 15px;">
            <h6>{{ $current_menu->name }}</h6>
            </div>
            <div class="col-md-9 float-right">
                {{-- <i class="fa fa-search"></i>  --}}
                <button class="btn btn-primary float-right"><a href="{{ URL::to('diner/menu/'.$current_menu->id.'/item/create?venue_id='.$venue_id) }}">Add Item</a></button>
                <button class="btn btn-primary float-right mr-2"><a href="{{ URL::to('diner/menu/'.$current_menu->id.'/sub-menu/create?venue_id='.$venue_id) }}">Add Menu</a></button>
            </div>
        </div>
        <div class="row menus-fBox" id="sortable">
            @if($current_menu)
                @if(count($arrangmentall))
                    @foreach($arrangmentall as $key => $arrangment)
                        @if ($arrangment->type == 'Item')
                            <div class="col-md-4 menu-block" id="card_{{ $arrangment->id }}" data-type="{{ $arrangment->type }}" data-id="{{ $arrangment->arrangmentable_id }}">
                                <div class="box-img">
                                    <a href="{{ $arrangment->showUrl }}"><img src="{{ $arrangment->image }}" width="100%"></a>
                                    <div class="dropdown fly">
                                        <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{  $arrangment->editUrl }}"><i class="far fa-edit"></i>  Edit </a>
                                            <a class="dropdown-item" href="#"><i class="far fa-copy"></i> Duplicate</a>
                                            <a class="dropdown-item movebtnclass" data-toggle="modal" data-target="#MoveModal" data-id="{{ $arrangment->col_id }}" data-type="{{ $arrangment->type }}"><i class="fas fa-share"></i> Move</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-box">
                                    <div class="row">
                                        <div class="col-md-6 col-6 text-left"><h6><a href="{{ $arrangment->showUrl }}">{{ $arrangment->name }}</a></h6></div>
                                        <div class="col-md-6 col-6 text-right"><input type="checkbox" {{ $arrangment->is_active? 'checked': '' }} class="animate-checkbox active_deactivate_item_btn" data-id="{{ $arrangment->col_id }}"></div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-4 menu-block" id="card_{{ $arrangment->id }}" data-type="{{ $arrangment->type }}" data-id="{{ $arrangment->arrangmentable_id }}">
                                <div class="box-img">
                                    <a href="{{ $arrangment->showUrl }}"><img src="{{ $arrangment->image }}" width="100%"></a>
                                    <div class="dropdown fly">
                                        <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{  $arrangment->editUrl }}"><i class="far fa-edit"></i>  Edit </a>
                                            <a class="dropdown-item movebtnclass" data-toggle="modal" data-target="#MoveModal" data-id="{{ $arrangment->col_id }}" data-type="{{ $arrangment->type }}"><i class="fas fa-share"></i> Move</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-box">
                                    <div class="row">
                                        <div class="col-md-6 col-6 text-left"><h6><a href="{{ $arrangment->showUrl }}">{{ $arrangment->name }}</a></h6></div>
                                        <div class="col-md-6 col-6 text-right"><input type="checkbox" {{ $arrangment->is_active? 'checked': '' }} class="animate-checkbox active_deactivate_btn" data-id="{{ $arrangment->col_id }}"></div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            @endif
            
           
            <div class="col-md-4">
                <div class="box-img text-center" style="border: 1px solid #999; border-bottom: 0;">
                    <a href="{{ URL::to('diner/menu/'.$current_menu->id.'/sub-menu/create?venue_id='.$venue_id) }}"><i class="fas fa-plus" style="line-height: 6; cursor: pointer; font-size: 25px; color: #9b9b9b"></i></a>
                </div>
                <div class="back-box" style="background-color: transparent; border: 1px solid #999">
                    <div class="row">
                        <div class="col-md-6 col-6 text-left"><span><a href="{{ URL::to('diner/menu/'.$current_menu->id.'/sub-menu/create?venue_id='.$venue_id) }}" style="color: #9b9b9b">Add Menu </a></span></div>
                        <div class="col-md-6 col-6 text-right"></div>
                    </div>
                </div>
                <br><br>
            </div>
            <div class="col-md-4">
                <div class="box-img text-center" style="border: 1px solid #999; border-bottom: 0;">
                    <a href="{{ URL::to('diner/menu/'.$current_menu->id.'/item/create') }}"><i class="fas fa-plus" style="line-height: 6; cursor: pointer; font-size: 25px; color: #9b9b9b"></i></a>
                </div>
                <div class="back-box" style="background-color: transparent; border: 1px solid #999">
                    <div class="row">
                        <div class="col-md-6 col-6 text-left"><span><a href="{{ URL::to('diner/menu/'.$current_menu->id.'/item/create') }}" style="color: #9b9b9b">Add Item </a></span></div>
                        <div class="col-md-6 col-6 text-right"></div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>  
    </div>  

    @include('diner.menu.movemodel')
    {{--
                    
                    <div class="col-md-4 col-4 menu-block" id="card_{{ $arrangment->id }}" data-type="{{ $arrangment->type }}" data-id="{{ $arrangment->arrangmentable_id }}">
                        <div class="btn-group">
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" >
                                        @if ($arrangment->type == 'Menu')
                                            {{Form::open(array('url'=>route('diner.venue.import',['venue_id'=>$venue_id,'menu_id' => $arrangment->arrangmentable_id, 'parent_id' => $arrangment->arrangmentable->parent_id]) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
                                        @else
                                            {{Form::open(array('url'=>route('diner.venue.import',['type' => $arrangment->type, 'venue_id'=>$venue_id,'item_id' => $arrangment->arrangmentable_id, 'menu_id' => $arrangment->arrangmentable->menu_id]) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
                                        @endif
                                                <button type="submit" class="btn btn-primary">Duplicate</button>
                                            {{Form::close()}}
                                </a>
                                <a class="dropdown-item" >
                                    <button class="btn btn-primary movebtn" data-type="{{ $arrangment->type }}" data-id="{{ $arrangment->arrangmentable_id }}" type="button" data-toggle="modal" data-target="#exampleModal">
                                        Move
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="cat-block {{$arrangment->is_active == 0 ? 'cat-hidden' : '' }}" data-url="{{ $arrangment->showUrl }}"
                                data-id="{{$arrangment->arrangmentable_id}}"  data-last="{{ $arrangment->is_last_child}}" >
                            <div class="cat-block-inner">
                                <img src="{{ $arrangment->image }}">
                            </div>

                            <div class="cat-block-title">
                                <h4>{{  $arrangment->name}}</h4>
                                <a class="text-muted"
                                    href="{{  $arrangment->editUrl }}">
                                    <i class="fas fa-pencil-alt"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    @endif --}}
    {{-- <div class="row">
        <a href="#!" onclick="checkout()" data-product="565725">Buy Now!</a>
    </div> --}}

    
    
@stop

@section('scripts')
    <!-- Deactivate Alert -->
    <div class="modal fade" id="deactivateAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p id="modal_msg">Are you sure you want
                        to {{$current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}} {{$current_menu->name}}?</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="button" id="active_deactivate_btn"
                            class="dark-btn">{{$current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}}
                        MENU
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {{Form::open(['route'=>['diner.menu.destroy' , $current_menu->id ] , 'method'=>'delete'])}}

            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p class="m-0">Are you sure you want to delete {{$current_menu->name}}?</p>
                    <small class="text-danger">This cannot be undone</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="dark-btn">DELETE MENU</button>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>

    <!-- Move Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Move Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                        {{Form::open(array('url'=>route('diner.menu.move',['venue_id'=>$venue_id]) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
                            <input type="hidden" name="menu_id" id="menu_id_input" >
                            <input type="hidden" name="type" id="type_input" >
                            <div class="row">
                                    <div class="form-group col-12 floatlabel mb-4">
                                    <select name="parent_id" id="" class="form-control">
                                    @foreach (\App\Http\Models\Menu::where('customer_id', $venue_id)->where('is_active', 1)->get() as $item)
                                        @if ($current_menu ->id != $item->id)
                                            <option value="{{ $item->id }}">
                                                    <div class="card text-center">
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{ $item->name }}</h5>
                                                        </div>
                                                        <div class="card-footer text-muted">
                                                            ({{ $item->MenuParent->name ?? 'Main Menu' }})
                                                        </div>
                                                    </div>
                                            </option>                                                                                    
                                        @endif
                                    @endforeach
                                    </select>
                                    
                                </div>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        {{Form::close()}}
                </div>
                <div class="modal-footer">
                
                </div>
            </div>
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

        $('.active_deactivate_btn').click(function () {
            var id = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/menu/hide_show')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: id
                }
            }).done(function (data) {
            });
        });
        $('.active_deactivate_item_btn').click(function () {
            var id = $(this).attr('data-id');
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/item/hide_show')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: id
                }
            }).done(function (data) {
            });
        });


        $(".cat-block").click(function () {
            id = $(this).attr('data-id');
            lastChild = $(this).attr('data-last');
            venueID = '{{request()->venue_id}}';
            var url = $(this).attr('data-url');
            //url = lastChild ? '{{URL::to('diner/menu')}}/' + id + '/item?venue_id={{request()->venue_id}}' : '{{URL::to('diner/menu')}}/' + id + '?venue_id={{request()->venue_id}}';
            window.open(url , '_self')
        })

        $(".cat-item-block").click(function () {
            id = $(this).attr('data-id');
            menuID = $(this).attr('data-menuid');
            window.open('{{URL::to('diner/menu')}}/' + menuID + '/item/' + id , '_self')
        })

        $(".editable-heading input").on('focus', function () {
            $(this).parent().find('a').show();
        });

        $(".editable-heading input").on('change', function () {
            var self = $(this);
            var title = $(this).val();
            var id = $(this).data('id')
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/section/create')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    menu_id: "{{$current_menu->id}}",
                    title: title,
                    id: id
                }
            }).done(function (data) {
                var data = jQuery.parseJSON(data);
                if (data.status == 2) {
                    self.parent().find("p").addClass('alert alert-danger text-center').html(data.error);
                    showMessages();
                } else if (data.status == 1) {
                    self.parent().find("p").addClass('alert alert-success text-center').html(data.message);
                    showMessages();
                }
            });
            //slef.next('a').hide();

        });

        // $(".editable-heading a").on('click', function (e) {
        //     e.preventDefault();
        //     $(this).parent('div').hide();
        // });

        $(".add-category-btn").click(function () {
            section_id = $(this).attr('data-section-id')
            window.open("{{URL::to('diner/menu/'.$current_menu->id.'/sub-menu/create')}}/?section_id=" + section_id, '_self')
        });

        $(document).on("click", ".editable-heading a", function () {
            var id = $(this).data('id');
            self = $(this);
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/section/remove')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    menu_id: "{{$current_menu->id}}",
                    id: id
                }
            }).done(function (data) {
                var data = jQuery.parseJSON(data);
                if (data.status == 2) {
                    self.parent().find("p").addClass('alert alert-danger text-center').html(data.error);
                    showMessages();
                } else if (data.status == 1) {
                    self.parent().find("p").addClass('alert alert-success text-center').html(data.message);
                    self.closest('div.section-block').find('div.menu-block').each(function () {
                        self.closest('div.section-block').prev().find('button.add-category-btn').before($(this));
                    });
                    setTimeout(function () {
                        self.closest('div.section-block').remove();
                    }, 3000);

                }
            });

        });
    </script>

    <script>
        $( function() {
            $( "#sortable" ).sortable({
                stop: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    var current_menu = $('#current_menu').val();
                    $.get('/diner/arrange/'+current_menu,{data:data}, function(data){
                        // console.log(data);
                    });
                }
            });
            $( "#sortable" ).disableSelection();
        } );

        $(document).on('click', '.movebtn', function(event){
            $('#menu_id_input').val($(this).attr('data-id'));
            $('#type_input').val($(this).attr('data-type'));
        });
    </script>

    <script src="https://cdn.paddle.com/paddle/paddle.js"></script>
    <script type="text/javascript">
        Paddle.Setup({ vendor: 52571 });
        function checkout(){
            Paddle.Checkout.open({
            product: 566512,
            successCallback: function(data) {
                var checkoutId = data.checkout.id;
                
                Paddle.Order.details(checkoutId, function(data) {
                
                console.log(data);
                });
            }
        });
        }




        
    </script> 

@stop
