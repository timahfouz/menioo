@extends('diner.layouts.master')

@section('content')
<div class="container">
    <div class="row row-title">
        <div class="col-md-6 text-left"><h6>Menus</h6></div>
        <div class="col-md-6 text-right"> <button class="btn btn-primary"><a href="{{ URL::to('diner/menu/create?venue_id='.$venue_id)}}">New Menu</a></button></div>
    </div>
    <div class="row menus-fBox">
        @if($menus)
            @foreach($menus as $menu)
                <div class="col-md-6">
                    <div class="box-img">
                        <a href="{{ URL::to('diner/menu/' . $menu->id . '?venue_id='.$venue_id)}}"><img src="{{  $menu->FullThumbnailImagePath('500X500')}}" width="100%"></a>
                        <div class="dropdown fly">
                            <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent">
                                <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ URL::to('diner/menu/' . $menu->id . '/edit?venue_id='.$venue_id)}}"><i class="far fa-edit"></i>  Edit </a>
                                {{-- <a class="dropdown-item" href="#"><i class="fas fa-trash"></i> Delete</a> --}}
                                {{-- <a class="dropdown-item" href="{{ URL::to('diner/menu/' . $menu->id . '/duplicate?venue_id='.$venue_id)}}"><i class="far fa-copy"></i> Duplicate</a> --}}
                                <a class="dropdown-item movebtnclass" data-toggle="modal" data-target="#MoveModal" data-id="{{ $menu->id }}" data-type="Menu"><i class="fas fa-share"></i> Move</a>
                            </div>
                        </div>
                    </div>
                    <div class="back-box">
                        <div class="row">
                            <div class="col-md-6 col-6 text-left"><h6><a href="{{ URL::to('diner/menu/' . $menu->id . '?venue_id='.$venue_id)}}">{{  $menu->name}}</a></h6></div>
                            <div class="col-md-6 col-6 text-right"><input type="checkbox" {{ $menu->is_active? 'checked': '' }} class="animate-checkbox active_deactivate_btn" data-id="{{ $menu->id }}"></div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        <div class="col-md-6">
            <div class="box-img text-center" style="border: 1px solid #999; border-bottom: 0;">
                <a href="{{ URL::to('diner/menu/create?venue_id='.$venue_id)}}"><i class="fas fa-plus" style="line-height: 6; color: #444; cursor: pointer; font-size: 25px; color: #9b9b9b"></i></a>
            </div>
            <div class="back-box" style="background-color: transparent; border: 1px solid #999">
                <div class="row">
                    <div class="col-md-6 col-6 text-left"><h6 ><a href="{{ URL::to('diner/menu/create?venue_id='.$venue_id)}}" style="color: #9b9b9b">Add Menu</a></h6></div>
                    <div class="col-md-6 col-6 text-right"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
    @include('diner.menu.movemodel')

    
@stop
@section('scripts')
    <script>
        $(".cat-block").click(function () {
            id = $(this).attr('data-id');
            venuID = "{{$venue_id ? $venue_id : 0}}"
            url = venuID === '0' ? '{{URL::to('diner/menu')  }}/' + id : '{{URL::to('diner/menu')  }}/' + id + '?venue_id='+{{$venue_id}};
            window.open(url , '_self')
        })
    </script>
@stop
