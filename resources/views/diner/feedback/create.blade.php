@extends('diner.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('diner/feedback') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">

        <input type="hidden" name="customer_id" value="{{$customer_id}}" />


        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="form_name">Add a form name</label>
            <input type="text" data-validation="required" name="form_name" value="{{old('form_name')}}" id="form_name"
                   class="form-control"/>
        </div>

        <div class="col-8 form-group floatlabel mb-4">
            <label class="label" for="form_header">Add a form header</label>
            <input type="text" data-validation="required" name="form_header" value="{{old('form_header')}}" id="form_header"
                   class="form-control"/>
        </div>


        <div class="col-12 form-group" id="questions">
            <div class="form-group question">
                <input type="text" name="questions[0][content]" class="form-control col-md-5 float-md-left" placeholder="Type your question">
                <div class="form-group col-md-3 float-md-left">
                    <label class="label col-md-4 float-md-left" for="form_header">Question Type</label>
                    <select type="text" class="form-control col-md-8 col-md-push-1 float-md-left" name="questions[0][type]">
                        <option value="text">Text</option>
                        <option value="rating">Rating</option>
                        <option value="boolean">Yes/No</option>
                    </select>
                </div>
                <div class="form-group col-md-3 float-md-left">
                    <input type="checkbox" name="questions[0][mandatory]" class="form-control col-md-1 float-md-left">
                    <label class="label col-md-11 float-md-left" for="form_header" style="margin-top: 7px;">Mandatory Answer</label>
                </div>
            </div>
        </div>
        <input onclick="addQuestion()" type="button" class="btn btn-danger col-md-2 float-md-left" value="+ Add a Question"/>

    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD FEEDBACK FORM</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <style>
        #content_option_image,#content_option_video, #trigger_option_item {display: none;}
        #questions {
            border: 1px solid #eaeaea;
            padding: 10px;
            border-radius: 10px;
        }
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

    </script>

    <script>
        var questionIndex = 0;
        function addQuestion(){
            questionIndex ++;
            var html = `<div class="form-group question">
                <input type="text" name="questions[`+questionIndex+`][content]" class="form-control col-md-5 float-md-left" placeholder="Type your question">
                <div class="form-group col-md-3 float-md-left">
                    <label class="label col-md-4 float-md-left" for="form_header">Question Type</label>
                    <select type="text" class="form-control col-md-8 col-md-push-1 float-md-left" name="questions[`+questionIndex+`][type]">
                        <option value="text">Text</option>
                        <option value="rating">Rating</option>
                        <option value="boolean">Yes/No</option>
                    </select>
                </div>
                <div class="form-group col-md-3 float-md-left">
                    <input type="checkbox" name="questions[`+questionIndex+`][mandatory]" class="form-control col-md-1 float-md-left">
                    <label class="label col-md-11 float-md-left" for="form_header" style="margin-top: 7px;">Mandatory Answer</label>
                </div>
                <input onclick="remove(this)" type="button" class="btn btn-danger col-md-1 float-md-left" value="Delete"/>
            </div>`;

            $('#questions').append(html);
        }

        function remove(e) {
            e.closest('div.question').remove();
        }
        // $('.removeItem').click(function () {
        //     this.closest('div.question').remove();
        // })
    </script>

@stop
