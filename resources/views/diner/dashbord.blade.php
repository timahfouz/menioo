@extends('diner.layouts.master')


@section('content')

    <div class="container">
        <div class="row row-title">
            <div class="col-md-6 text-left"><h6>Dashboard</h6></div>
            <div class="col-md-6 text-right"><label>Date:</label> <input type="date"></div>
        </div>
        <div class="row row-columm">
            <div class="col-md-3">
                <h6>Connected Devices</h6>
                <div class="img text-center">
                    <div class="chart" data-percent="70" data-scale-color="#ffb400"><strong>7</strong>/10</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <h6>{{ $open_servay }}</h6>
                        <p>Open Survey Results</p>
                    </div>
                    <div class="col-md-12">
                        <h6>{{ $running_campain  }}</h6>
                        <p>Running Campaigns</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <h6>{{ $open_orders }}</h6>
                <br><br><br>
                <p>Open Orders</p>
            </div>
        </div>
        <div class="row last-row">
            <div class="col-md-12">
                <h6>Total Orders This Week</h6>
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>

    <!--***************** Start Top View **************************-->
    <div class="top-view">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-5">
                    <h5 class="sm">Top Viewed Items</h5>
                </div>
                <div class="col-md-6 col-7 text-right">
                    <form id="topViewsForm" action="">
                        <span>Sort by</span> &nbsp;&nbsp;
                        <select name="top_views" onchange="document.getElementById('topViewsForm').submit()">
                            <option value="1">Top Viewed</option>
                            <option {{ request()->filled('top_views') && request()->top_views == 0 ? 'selected':'' }} value="0">Less Viewed</option>
                        </select>
                    </form>
                </div>
            </div>
            @foreach($items_viewed as $item)
            <div class="row">
                <div class="col-md-8 col-11">
                    <div class="row">
                        <div class="col-md-2 col-3">
                            <div class="top-view-image">
                                <img src="{{ $item->image_path }}" width="100%">
                            </div>
                        </div>
                        <div class="col-md-10 col-9 top-view-text">
                            <h6>{{ $item->name }}</h6>
                            <small>{{ $item->description }}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-1 text-right">
                    <span class="num"><b>{{ $item->views_count }}</b></span>
                </div>
            </div>
            @endforeach

        </div>
    </div>

    <div class="donut">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!--
                           canvas
                            <canvas id="mycanvas"></canvas>
                    -->
                    <h5>Revenue Share</h5>
                    <div class="smll" style="margin-top: -10px;">
                        <small>Revenue Share Between sections</small>
                    </div>
                    <br>
                    <img src="{{ asset('imgs/pauntchart.png') }}" width="100%" alt="">
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-8 col-8"><h5>Ticket Closed</h5></div>
                        <div class="col-md-4 col-4 text-right">{{ $closed_orders }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-8"><h5>Items Served</h5></div>
                        <div class="col-md-4 col-4 text-right">{{ $item_Selled }}</div>
                    </div>
                    {{--<div class="row">
                        <div class="col-md-8 col-8"><h5>Revenue</h5></div>
                        <div class="col-md-4 col-4 text-right">EGP1500</div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-8"><h5>Avg Ticket Size</h5></div>
                        <div class="col-md-4 col-4 text-right">Medium</div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-8"><h5>Avg Ticket Time</h5></div>
                        <div class="col-md-4 col-4 text-right">12 Day</div>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    <script src=" {{ asset('js/jquery.easypiechart.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script>
        let myChart = document.getElementById('myChart').getContext('2d');

        var days = ['Sat','Sun','Mon','Tue','Wed','Thu','Fri'];
        var vals = new Array();
        console.log(days);
        @foreach($days as $day)
            vals.push({{$orders_chart[$day]}})
        @endforeach
        console.log(days);

        let massPopChart = new Chart(myChart, {
            type:'bar',
            data:{
                labels:days,
                datasets:[{
                    label:'Population',
                    data:vals,
                    backgroundColor:'#ce00ff',
                    borderWidth:1,
                    borderColor:'#fff',
                    hoverBorderWidth:2,
                    hoverBorderColor:'#fff'
                }]
            },
            option:{
                legend:{
                    display:false,
                }
            }
        });
    </script>
@endsection
