@extends('diner.layouts.master')




@section('content')
    <div class="row">
        <div class="col sm-2" >
            <button style="float: right;" class="btn btn-danger text-white deleteall" >Delete</button>
        </div>
    </div>
    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>
                    <div class="custom-control custom-checkbox">
                        <input name="" type="checkbox" class="custom-control-input" id="checkbox_all">
                        <label class="custom-control-label" for="checkbox_all"></label>
                    </div>
                </th>
                <th>Model</th>
                <th>UDID</th>
                <th>App Version</th>
                <th>OS</th>
                <th>Last Connected</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
        
                @foreach($devices as $device)
                    <tr class="viewPaln">
                        <td>
                            <div class="custom-control custom-checkbox">
                                <input value="{{ $device->id }}" name="devices[]" type="checkbox" class="custom-control-input checkboxinput" id="checkbox_{{ $device->id }}">
                                <label class="custom-control-label" for="checkbox_{{ $device->id }}"></label>
                            </div>
                        </td>
                        
                        <td>{{ $device->model }}</td>
                        <td>{{ $device->udid }}</td>
                        <td>{{ $device->app_version }}</td>
                        <td>{{ $device->os }}</td>
                        <td>{{ $device->last_connected }}</td>
                        

                        <td>
                            <button class="btn btn-danger deletedevice" data-id="{{ $device->id }}"  data-toggle="modal" onclick="return confirm('Are you sure you want to delete this device')" data-target="#delAlert_{{$device->id}}" class="">DELETE</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@stop
@section('scripts')
    <script>
        $(document).on('click','#checkbox_all', function(event){
            if(this.checked){
                $('.checkboxinput').each(function(){
                    this.checked = true;
                });
                
            }else{
                $('.checkboxinput').each(function(){
                    this.checked = false;
                });

            }
        });

        $(document).on('click', '.deletedevice', function(event){
            var device_id = $(this).attr('data-id');
            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/diner/devices/'+device_id,
                    type: 'DELETE',
                    success: function(result) {
                        alert('Removed Successfully');
                        location.reload();
                    }
                });
        });

        $(document).on('click', '.deleteall', function(){
            var devices = [];
            $.each($(".checkboxinput:checkbox:checked"), function(){            
                devices.push($(this).val());
            });
            console.log(devices)
            $.get('/diner/devices/deleteall/'+devices ,function(data){
                
                if(data.success){
                    alert(data.success)
                    location.reload();
                }
                else{
                    alert(data.fail);

                }
            })
        });
    </script>
    
@stop
