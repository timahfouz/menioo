@extends('diner.layouts.master')

@section('content')
    {{Form::open(array('url'=>array('diner/menu/'.$parent_id.'/sub-menu/'.$current_menu->id.'?venue_id='.$venue_id) ,'method'=>'PUT','files'=>'true','enctype'=>'multipart/form-data'))}}


    <div class="row">
        <div class="col-md-6 col-12">
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="menu-name">Menu Name</label>
                <input type="text" name="name" value="{{old('name',$current_menu->name)}}"
                       data-validation="required server"
                       data-validation-url="{{URL::to('diner/ajax/validation')}}"
                       data-validation-req-params='{"model":"menu","_token":"{{csrf_token()}}","parent_id":"{{$parent_id}}","id":"{{$current_menu->id}}"}'
                       id="menu-name"
                       class="form-control"/>
            </div>
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="description">Description</label>
                <textarea name="description" id="description"
                          class="form-control">{{old('description',$current_menu->description)}}</textarea>
            </div>
            <div class="row m-0">
                <div class="col-6 form-group mb-4">
                    <select class="form-control no-background pl-0" data-validation="required"
                            name="display_as" id="display_as">
                        <option value="" selected>Display As</option>
                        <option value="grid" {{old('display_as',$current_menu->display_as)=='grid' ? 'selected':''}}>
                            Grid
                        </option>
                        <option value="list" {{old('display_as',$current_menu->display_as)=='list' ? 'selected':''}}>
                            List
                        </option>
                    </select>
                </div>
                <div class="col-6 form-group mb-4">
                    <select class="form-control no-background pl-0"
                            name="num_of_rows" id="num_of_rows">
                        <option value="" disabled selected>Number of Columns</option>
                        <option value="2" {{old('num_of_rows',$current_menu->num_of_rows)=='2' ? 'selected':''}}>2
                        </option>
                        <option value="4" {{old('num_of_rows',$current_menu->num_of_rows)=='4' ? 'selected':''}}>4
                        </option>
                        <option value="6" {{old('num_of_rows',$current_menu->num_of_rows)=='6' ? 'selected':''}}>6
                        </option>
                        <option value="8" {{old('num_of_rows',$current_menu->num_of_rows)=='8' ? 'selected':''}}>8
                        </option>
                    </select>
                </div>
            </div>
            <div class="form-group col-12 mb-4">
                <select class="form-control no-background pl-0" name="grid_title_position" id="grid_title_position">
                    <option value="" disabled selected>Grid View Title Position</option>
                    <option value="top" {{old('grid_title_position',$current_menu->grid_title_position)=='top' ? 'selected':''}}>
                        Top
                    </option>
                    <option value="bottom" {{old('grid_title_position',$current_menu->grid_title_position)=='bottom' ? 'selected':''}}>
                        Bottom
                    </option>
                </select>
            </div>
            <div class="form-group col-12 mb-4">
                <label>Display Similar Items</label>
                <div>
                    <label class="switch">
                        <input type="checkbox" class="primary" name="display_similar_items"
                               {{old('display_similar_items',$current_menu->display_similar_items) == 1? 'checked':''}} value="1">
                        <span class="slider round"></span>
                    </label>
                </div>
                <small class="text-muted">Display other items of the Category on each Item
                    Page.
                </small>
            </div>
            <div class="row m-0">
                <div class="col-md-6 col-12 form-group mb-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="mark_as_new" value="1"
                               id="mark_as_new" {{old('mark_as_new',$current_menu->mark_as_new) == 1? 'checked':''}}>
                        <label class="custom-control-label" for="mark_as_new">Mark Menu as
                            New</label>
                    </div>
                </div>
                <div class="col-md-6 col-12 form-group mb-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="mark_as_signature" value="1"
                               id="mark_as_signature" {{old('mark_as_signature',$current_menu->mark_as_signature) == 1? 'checked':''}}>
                        <label class="custom-control-label" for="mark_as_signature">Mark Menu as
                            Signature</label>
                    </div>
                </div>
            </div>
            <div class="form-group col-12 floatlabel mb-4">
                <label class="label" for="notes">Add notes here..</label>
                <textarea name="additional_notes" id="notes"
                          class="form-control">{{old('additional_notes',$current_menu->additional_notes)}}</textarea>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="uploaded-pic">
                <img src="{{$current_menu->FullThumbnailImagePath('517X255')}}">
                <button class="change-pic-btn">Change Picture</button>
                <input type="file" name="file"  title="Upload picture" class="custom-file-input">
            </div>

            <div class="uploaded-video">
                <input type="file" name="video" title="Add a Video" class="custom-file-input">
                <button class="plain-btn btn-block"><i class="fas fa-film"></i> Add a
                    video</button>
            </div>
        </div>


    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">SAVE Menu</button>
        <button type="button" data-toggle="modal" data-target="#deactivateAlert" id="active_deactivate_modal_btn"
                class="plain-btn text-muted"><i
                    class="far fa-eye-slash"></i> {{$current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}}
        </button>
        <button type="button" data-toggle="modal" data-target="#delAlert"
                class="plain-btn text-danger"><i class="far fa-trash-alt"></i> DELETE
        </button>
    </div>

    {{Form::close()}}
@stop

@section('scripts')
    <!-- Deactivate Alert -->
    <div class="modal fade" id="deactivateAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p id="modal_msg">Are you sure you want
                        to {{$current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}} {{$current_menu->name}}?</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="button" id="active_deactivate_btn"
                            class="dark-btn">{{$current_menu->is_active == 1 ? 'HIDE' : 'SHOW'}}
                        MENU
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Alert -->
    <div class="modal fade" id="delAlert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {{Form::open(['route'=>['diner.menu.destroy' , $current_menu->id ] , 'method'=>'delete'])}}

            <div class="modal-content text-center">
                <div class="modal-body">
                    <h5 class="text-center">ARE YOU SURE?</h5>
                    <p class="m-0">Are you sure you want to delete {{$current_menu->name}}?</p>
                    <small class="text-danger">This cannot be undone</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="plain-btn" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="dark-btn">DELETE MENU</button>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#display_as").trigger("change");
        });

        $.validate({modules: 'security'});

        $('#display_as').change(function () {
            val = $(this).val();
            if (val == 'grid') {
                $('#num_of_rows').attr('data-validation', 'required');
                $('#grid_title_position').attr('data-validation', 'required');
                $('#num_of_rows').removeClass('d-none');
                $('#grid_title_position').removeClass('d-none');
            } else {
                $('#num_of_rows').val('');
                $('#grid_title_position').val('');
                $('#num_of_rows').addClass('d-none');
                $('#grid_title_position').addClass('d-none');
                $('#num_of_rows').removeAttr('data-validation');
                $('#grid_title_position').removeAttr('data-validation');
            }
        })


        $('#active_deactivate_btn').click(function () {
            $.ajax({
                method: "POST",
                url: "{{URL::to('diner/ajax/menu/hide_show')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: "{{$current_menu->id}}"
                }
            }).done(function (data) {

                var data = jQuery.parseJSON(data);
                if (data.status == 2) {

                    html = '<div class="alert alert-danger text-center"> +data.error+</div>';
                    $('#active_deactivate_btn .modal-body').append(html);

                } else if (data.status == 1) {

                    html = data.data.is_active == 1 ? 'HIDE' : 'SHOW';

                    $('#active_deactivate_modal_btn').html('<i class="far fa-eye-slash"></i>' + html);
                    $('#active_deactivate_btn').html(html + ' MENU');
                    $('#active_deactivate_btn .modal-body .alert-danger').remove();
                    $('#deactivateAlert').modal('hide');
                    $('#modal_msg').text("Are you sure you want to " + html + " {{$current_menu->name}}?");

                }
            });
        });
    </script>


@stop
