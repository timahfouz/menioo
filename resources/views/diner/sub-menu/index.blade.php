@extends('diner.layouts.master')

@section('content')

    <div class="row">
        @if($menus)
            @foreach($menus as $menu)
                <div class="col-md-4 col-12">
                    <div class="cat-block">
                        <div class="cat-block-inner">
                            <img src="{{$menu->FullThumbnailImagePath('500X500')}}">
                        </div>

                        <div class="cat-block-title">
                            <h4>{{$menu->name}}</h4>
                            <a class="text-muted" href="{{ URL::to('diner/menu/' . $menu->id . '/edit')}}">
                                <i class="fas fa-pencil-alt"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@stop