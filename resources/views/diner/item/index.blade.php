@extends('diner.layouts.master')

@section('content')

    <div class="row">
        @if($items)
            @foreach($items as $item)
                <div class="col-md-4 col-12">
                    <div class="cat-block {{$item->is_active == 0 ? 'cat-hidden' : '' }}" data-id="{{$item->id}}" data-menuid="{{$item->menu_id}}">
                        <div class="cat-block-inner">
                            <img src="{{$item->item_image}}">
                        </div>

                        <div class="cat-block-title">
                            <h4>{{$item->name}}</h4>
                            <a class="text-muted" href="{{ URL::to('diner/menu/' . $item->menu_id . '/item/'.$item->id.'/edit')}}">
                                <i class="fas fa-pencil-alt"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@stop


@section('scripts')
    <script>
        $(".cat-block").click(function () {
            id = $(this).attr('data-id');
            menuID = $(this).attr('data-menuid');
            window.open('{{URL::to('diner/menu')}}/' + menuID + '/item/' + id , '_self')
        })
    </script>
@stop
