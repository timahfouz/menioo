<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" media="screen and (device-height: 620px)" />
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<style type="text/css">
		#app {
			/*margin-top: 10px;*/
			/*margin-left: 4%;*/
			height: 100vh;
			width:  100vw;
			background-image: url("{{ asset('images/nexus-bg.png') }}");
			background-repeat: no-repeat;
			background-size: 100% 100%;
		}
	</style>
    <script> window.Laravel = { csrf_token : '{{csrf_token()}}' }</script>
</head>
<body>
	<div class="col-md-12" id="app">

      <router-view :link="'{{ $resource }}'" :key="$route.fullPath"></router-view>

 	</div>
	<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
 	<script src="{{ asset('js/preview-app.js') }}"></script>
</body>
</html>
