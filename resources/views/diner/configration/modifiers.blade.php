@extends('diner.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12" id="sideDishes">
            <h3>Side Dishes</h3>
            {{  Form::open(array('url'=>array('diner/sidedishes') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}            
                <div class="dishRow">
                    <div class="form-group col-3 floatlabel mb-4" id="nameDiv">
                        <input placeholder="Name" class="form-control" data-validation="required"
                        name="name" id="autocomplete"/>
                    </div>
                    <div class="form-group col-3 floatlabel mb-4">
                        <input type="number" name="price" placeholder="Add Price" min="1" data-validation="required" id="price"  class="form-control"/>
                    </div>
                    <div class="form-group col-4 floatlabel mb-4" id="imageDiv">
                        <input  type="file" name="image"  class="form-control"/>
                    </div>
                    <div class="form-group col-1 mb-4">
                        <button id="addbtn" type="submit" class="btn btn-primary float-right  ">
                            Add
                        </button>
                    </div>
                    <div class="form-group col-1 mb-4">
                        <button type="button" item-id="" id="deletebtn" class="btn btn-danger float-right" style="display:none;">
                            Delete
                        </button>
                    </div>
                </div>
            {{ Form::close() }}
            {{-- <div class="row">
                @foreach ($sideDishes as $sideDish)
                
                    <div class="col-md-3">
                        <div class="card" style="width: 243px;height: 280px;">
                            <img class="card-img-top" style="width:240px;height: 130px;" src="{{ $sideDish->FullThumbnailImagePath('300X300') }}" alt="">
                            <div class="card-body">
                                <h5 class="card-title">{{ $sideDish->name }}</h5>
                                <p class="card-text">{{ $sideDish->price }}</p>
                                
                                {{Form::open(['route'=>['diner.sidedishes.destroy', $sideDish->id] , 'method'=>'delete'])}}

                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are You sure ?')">Delete</button>                                    

                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                @endforeach
                
            </div> --}}
        </div>
        <div class="col-md-12" id="Options">
            <h3>Options</h3>
            {{  Form::open(array('url'=>array('diner/options') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}      
                    <div class="row">
                        <div class="form-group col-5 floatlabel mb-4" id="nameDiv">
                            {!! Form::select('type',['' => 'Type', 'Slider' => 'Slider', 'Checkbox' => 'Checkbox'], null, ['class' => 'form-control', 'data-validation' => 'required', 'id' => 'optiontype']) !!}
                        </div>
                        <div class="form-group col-5 floatlabel mb-4" id="nameDiv">
                            <input placeholder="Name" class="form-control" data-validation="required"
                            name="name" />
                        </div>
                        <div class="form-group col-1 mb-4">
                            <button  type="button" class="float-right btn-link btn btn-sm addvalue">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                        
                    </div>

                    <div class="optionAppend">
                        <div class="valueRow">
                            {{-- <div class="form-group col-5 floatlabel mb-4">
                                <input  type="text" name="value[]" placeholder="Value" class="form-control"/>
                            </div>
                            <div class="form-group col-5 floatlabel mb-4">
                                <input  type="number" step="0.01" min="1" name="price[]" placeholder="Price" class="form-control"/>
                            </div> --}}
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" item-id=""  class="btn btn-primary  float-right" >
                            Save
                        </button>
                    </div> 
                
            {{ Form::close() }}
            
        </div>
    </div>
@stop
@section('scripts')>
    <style>
        #sideDishes {
            border: 1px solid #eaeaea;
            padding: 10px;
            border-radius: 10px;
        }
        #Options {
            border: 1px solid #eaeaea;
            padding: 10px;
            border-radius: 10px;
        }
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

    <script>
        $.validate({modules: 'security'});
        $( function() {
            var availableTags = JSON.parse('{!! $sideDishes !!}');
            $( "#autocomplete" ).autocomplete({
                source: availableTags,
                select: function( event, ui ) {
                    $('#editImage').remove();
                    $('#price').val(ui.item.price);
                    $('#imageDiv').append('<img src="'+ui.item.photo+'" style="width: 120px;height: 80px;" id="editImage"></img>')
                    $('#deletebtn').attr('style','display:block;');
                    $('#deletebtn').attr('item-id',ui.item.id);
                    $('#addbtn').text('Edit');
                },
                change: function( event, ui ) {
                    var itemName = $(this).val();
                    var found = availableTags.find(function(element) {
                        return element.value == itemName;
                    });
                    if(found){
                        $('#nameDiv').append('<span class="help-block form-error">This name already exist You will Edit it</span>')
                        $('#addbtn').text('Edit');
                        $('#deletebtn').attr('item-id',ui.item.id);

                    }
                    else{
                        $('#nameDiv').children("span").remove();
                        $('#imageDiv').children("img").remove();
                        $('#deletebtn').attr('style','display:none;');
                        $('#price').val('');
                        $('#addbtn').text('Add');

                    }

                }
            });
        } );

        $(document).on('click', '#deletebtn', function(event){
            var ItemId = $(this).attr('item-id');
            $.get('/diner/sidedishes/destroy/'+ItemId,function(data){
                if(data.success){
                    alert(data.success);
                    location.reload();
                }
                else{
                    alert(data.Error);
                }
            });
        })
         
    </script>

    <script>
        $(document).on('click', '.addvalue' , function () {
           var type = $('#optiontype').val();
           if(type == 'Slider'){
                AppendSlider();
           }
           else if(type == 'Checkbox'){
            AppendCheckbox();
           }
           else{
               alert('Select type First');
           }
            
        });

        $(document).on('change', '#optiontype', function(){
            $(".optionAppend").html('');
            var type = $(this).val();
            if(type == 'Slider'){
                    AppendSlider();
            }
            else if(type == 'Checkbox'){
                AppendCheckbox();
            }
            else{
                alert('Select type First');
           }
        })

        function AppendSlider(){
            let clonevalue =
            `<div class="row valueRow">
                <div class="form-group col-4 floatlabel mb-4">
                    <input  type="text" name="values[]" placeholder="Value" class="form-control" data-validation="required"/>
                </div>
                <div class="form-group col-2mb-4">
                    <button type="button" class="plain-btn float-right removevalue text-danger">
                      <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;

            $(".optionAppend").append(clonevalue);

            // remove dish
            $(".removevalue").click(function () {
                this.closest('div.valueRow').remove();
            })
        }
        function AppendCheckbox(){
            
            let clonevalue =
            `<div class="row valueRow">
                <div class="form-group col-5 floatlabel mb-4">
                    <input  type="text" name="values[]" placeholder="Value" class="form-control" data-validation="required"/>
                </div>
                <div class="form-group col-5 floatlabel mb-4">
                    <input  type="number" step="0.01" min="1" name="prices[]" placeholder="Price" class="form-control" data-validation="required"/>
                </div>
                <div class="form-group col-2 mb-4">
                    <button type="button" class="plain-btn float-right removevalue text-danger">
                      <i class="far fa-trash-alt"></i>
                    </button>
                </div>
            </div>`;

            $(".optionAppend").append(clonevalue);

            // remove dish
            $(".removevalue").click(function () {
                this.closest('div.valueRow').remove();
            })
        }
    </script>
    
@stop
