@extends('diner.layouts.master')




@section('content')

        {{  Form::open(array('url'=>array('diner/configraton?customer_id='.$venue_id) ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}
            <div class="row">
                {{-- language --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Language</h3>
                    <div class="form-group col-12 floatlabel mb-4">
                        <label class="form-label" >Default Language</label>
                        <select class="form-control no-background" name="default_language" required name="menu_id">
                            <option value="">Select Language</option>

                            @foreach(config('translatable.locales') as $key => $lang)
                            <option value="{{  $key }}" {{ $config->default_language == $key ? 'selected':'' }}>{{  $lang}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 floatlabel mb-4">
                        <div class="custom-control custom-checkbox">
                            <input name="show_language_icon" type="checkbox" class="custom-control-input" id="show_language_icon" {{ $config->show_language_icon?  'checked':'' }}>
                            <label class="custom-control-label" for="show_language_icon">Show Language Icon</label>
                        </div>
                    </div>
                    <div  class="form-group col-12 floatlabel mb-4" >
                        <div id="other_languages" style="display: {{ $config->show_language_icon ? 'block':'none' }};">
                            <label>Other Languages</label>
                            {!! Form::select('other_languages[]',config('translatable.locales'),json_decode($config->other_languages)??null,['class'=>'form-control select2 no-background', 'multiple'=>'multiple']) !!}
                                
                        </div>
                        
                    </div>
                </div>
                {{-- Currency --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">System Currency</h3>
                    <div class="row">
                        <div class="form-check col-md-12">
                                <label  >Currency</label>
                                {!! Form::select('currency',config('app_conf.currency'),$config->currency??null,['class'=>'form-control select2 no-background']) !!}
                        </div>
                    </div>
                </div>
                {{-- Orintation --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Screen Orintation</h3>
                    <div class="row">
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="screen_orintation" id="Portrait" value="Portrait" checked>
                            <label class="form-check-label" for="Portrait">
                                Portrait
                            </label>
                            
                        </div>
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="screen_orintation" id="Landscape" value="Landscape" >
                            <label class="form-check-label" for="Landscape">
                                Landscape
                            </label>
                        </div>
                    </div>
                </div>
                {{-- Info Icon --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Display Info Icon</h3>
                    <div class="form-group col-12 floatlabel mb-4">
                        <div class="custom-control custom-checkbox">
                            <input name="show_info_icon" type="checkbox" class="custom-control-input" id="show_info_icon" {{ $config->show_info_icon ? 'checked':'' }}>
                            <label class="custom-control-label" for="show_info_icon">Show Info Icon</label>
                        </div>
                    </div>
                </div>
                {{-- Feedback Icon --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Display Feedback Icon</h3>
                    <div class="form-group col-12 floatlabel mb-4">
                        <div class="custom-control custom-checkbox">
                            <input name="show_feedback_icon" type="checkbox" class="custom-control-input" id="show_feedback_icon" {{ $config->show_feedback_icon ? 'checked':'' }}>
                            <label class="custom-control-label" for="show_feedback_icon">Show Feedback Icon</label>
                        </div>
                    </div>
                    <div  class="form-group col-12 floatlabel mb-4" >
                        <div id="other_languages" >
                            <label>Feedback Emails</label>
                            <input type="text" name="emails[]" value="{{ $config->emails  }}" data-role="tagsinput" id="emailtags" class="form-control" />
                        </div>
                        
                    </div>
                </div>
                {{-- Show Labels --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Display Labels</h3>
                    <div class="form-group col-12 floatlabel mb-4">
                        <div class="custom-control custom-checkbox">
                            <input name="show_labels" type="checkbox" class="custom-control-input" id="show_labels" {{ $config->show_labels ? 'checked':'' }}>
                            <label class="custom-control-label" for="show_labels">Show Tablet Labels</label>
                        </div>
                    </div>
                </div>
                {{-- Cal --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Calories</h3>
                    <div class="row">
                        <div class="form-check col-md-6">
                            <label  >Calories</label>
                            {!! Form::select('calories',['Cal' => 'Cal', 'Kcal' => 'Kcal'],$config->calories??null,['class'=>'form-control no-background']) !!}
                        </div>
                    </div>
                </div>
                {{-- Welcome Message --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Welcome Message</h3>
                    <div class="row">
                        <div class="form-check col-md-6">
                            <label  >Welcome Message</label>
                            {!! Form::text('welcome_message',$config->welcome_message??null,['class'=>'form-control no-background']) !!}
                        </div>
                    </div>
                </div>
                {{-- Font Style --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Font</h3>
                    <div class="row">
                        <div class="form-check col-md-6">
                            <label  >Font Style</label>
                            {!! Form::select('font_style',config('app_conf.font_style'),$config->font_style??null,['class'=>'form-control select2 no-background']) !!}
                        </div>
                    </div>
                </div>
               
                {{-- Waiter Login --}}
                <div class="col-md-6 col-12">
                    <h3 style="margin-top:50px;margin-bottom:50px;">Waiter Login</h3>
                    <div class="form-group col-12 floatlabel mb-4">
                        <div class="custom-control custom-checkbox">
                            <input name="waiter_login" type="checkbox" class="custom-control-input" id="waiter_login" {{ $config->waiter_login ? 'checked':'' }}>
                            <label class="custom-control-label" for="waiter_login">Login Icon</label>
                        </div>
                    </div>
                </div>
                {{-- Order Option --}}
                <div class="col-md-6 col-12" >
                    <fieldset {{ (isset($user_branch->plan->order_option) && $user_branch->plan->order_option) ? '': '' }}>
                        <h3 style="margin-top:50px;margin-bottom:50px;">Order Option</h3>
                        <div class="row">
                            <div class="form-check col-md-4">
                                <input class="form-check-input" type="radio" name="order_option" id="Browse" value="Browse" {{ $config->order_option == 'Browse' ? 'checked' :'' }}>
                                <label class="form-check-label" for="Browse">
                                    Browse
                                </label>
                            </div>
                            <div class="form-check col-md-4">
                                <input class="form-check-input" type="radio" name="order_option" id="Order" value="Order" {{ $config->order_option == 'Order' ? 'checked' :'' }}>
                                <label class="form-check-label" for="Order">
                                        Order
                                </label>

                            </div>
                        </div>
                    </fieldset>

                </div>
                <div class="col-md-6 col-12" >
                    <fieldset>
                        <h3 style="margin-top:50px;margin-bottom:50px;">Order Submmiting</h3>
                        <div class="row">
                            <div class="form-check col-md-4">
                                <input class="form-check-input" type="radio" name="order_submmiting" id="Waiter" value="Waiter" {{ $config->order_submmiting == 'Waiter' ? 'checked' :'' }}>
                                <label class="form-check-label" for="Waiter">
                                    Waiter
                                </label>
                            </div>
                            <div class="form-check col-md-4">
                                <input class="form-check-input" type="radio" name="order_submmiting" id="Guset" value="Guset" {{ $config->order_submmiting == 'Guset' ? 'checked' :'' }}>
                                <label class="form-check-label" for="Guset">
                                    Guset
                                </label>
                                
                            </div>
                        </div>
                    </fieldset>
                    
                </div>
            </div>
            <div class="form-group text-center mt-5">
                <button type="submit" class="dark-btn">Save</button>
            </div>
    
        {{  Form::close()}}


    
@stop
@section('scripts')
        
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
    <script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
   
    <script>
        $(".select2").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $(document).on('change', '#show_language_icon',function(event){
            if(this.checked) {
                $('#other_languages').css('display','block');
            }
            else{
                $('#other_languages').css('display','none');

            }
        });
        $(document).on('itemAdded','#emailtags', function(event){
            console.log('asdsadda');
        })
    </script>
    
@stop
