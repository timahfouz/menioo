<header>
    <div class="site-name">

        <a class="menu-toggle show" href="#"><i class="fas fa-bars"></i></a>
        <a href="">{{config('app.name')}}</a>
        &nbsp;&nbsp;

        <ul class="nav float-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-family: times;">
                  <img src="" alt="">
                  Venues
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="width: 200px;">
                    @if(count($venues))
                      <li><a href="{{route('diner.menu.index')}}" style="font-family: times;font-size: 14px;">Main Branch</a>  </li>
                      <li class="divider"></li>
                      @foreach($venues as $venue)
                      <li><a href="{{route('diner.menu.index',['venue_id'=>$venue->id])}}" style="float: left;font-family: times;font-size: 14px;"><img style="height:30px;width:30px;border-radius:50%;margin-right:5px;" src="{{$venue->FullLogoPath()}}" alt="">{{$venue->name}}</a>  <i onclick="editVenue({{$venue->id}},{{$venue_id}})" class="fa fa-edit" style="cursor: pointer;margin-left: 25px;margin-top: 5px;"></i></li>
                      @endforeach
                      <li class="divider"></li>
                    @endif
                    <li><a href="{{URL::to('diner/venue/create')}}" style="font-family: times;"><i class="icon-off"></i> Add Venue</a></li>
                </ul>
            </li>
            <li>
                <a style="margin-left: 30px;" target="_blank" href="{{route('diner.scan-qr',['venue_id' => Request::get('venue_id')])}}" style="font-family: times;">QR</a>
            </li>
        </ul>


    </div>



    <ul class="nav float-right">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, {{$current_user->name}}
                <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="{{URL::to('diner/logout')}}"><i class="icon-off"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
