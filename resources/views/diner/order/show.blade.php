@extends('diner.layouts.master')

@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')

    <div class="sort-option float-left">
      <p >Last Update: {{$current_order->updated_at}}</p>
      <p >Created: {{$current_order->created_at}}</p>
      <p >Waiter: {{$waiter->name}}</p>
    </div>
    <div class="sort-option float-right">
      <p >Service Charge: {{$current_order->service_charge}}</p>
      <p >Tax: {{$current_order->tax}}</p>
      <p >Tip: {{$current_order->tip}}</p>
      <p >Total Billed: {{$current_order->total_cost}}</p>
    </div>


    <div class="table-component table-responsive">
        <table class="table table-hover table-borderless">
            <thead>
            <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Total</th>
                <th>Sent</th>
            </tr>
            </thead>
            <tbody>
                @foreach($current_order->cart->items as $cartItem)
                <tr class="viewCustomer">
                    <td>{{$cartItem->item->name}}</td>
                    <td>{{$cartItem->quantity}}</td>
                    <td>{{$cartItem->item_price * $cartItem->quantity}}</td>
                    <td>{{$cartItem->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@stop
