@extends('admin.layouts.master')
@section('styling')
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/select2.min.css')}}">
@stop
@section('content')
    {{Form::open(array('url'=>array('admin/subscription') ,'method'=>'POST','files'=>'true','enctype'=>'multipart/form-data'))}}

    <div class="row">
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="order">PLAN ORDER</label>
            <input type="text" data-validation="required" name="order" value="{{old('order',1)}}" id="order"
                   class="form-control"/>
        </div>

        <div class="col-6 form-group mb-4">
            <select class="form-control no-background" name="type" id="type">
                @if(isset($types) && count($types))
                    @foreach($types as $type)
                        <option value="{{$type}}" {{old('type') == $type ? 'selected' : ''}}>{{$type}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-6 form-group mb-4">
            <select class="form-control no-background" name="plan_type" id="plan_type">
                <option value="day" >Daily</option>
                <option value="month" >Monthly</option>
                <option value="year" >Yearly</option>
                    
            </select>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">BILLING PERIOD</label>
            <input type="number" data-validation="required" name="billing_period" 
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="first-name">PLAN NAME</label>
            <input type="text" data-validation="required" name="name" value="{{old('name')}}" id="first-name"
                   class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4">
            <label class="label" for="price">PRICE</label>
            <input id="price" type="text" name="price" value="{{old('price')}}" class="form-control"/>
        </div>

        <div class="col-6 form-group floatlabel mb-4" id="tablets_number_container">
            <label class="label" for="tablets_number">NO. OF TABLETS</label>
            <input id="tablets_number" type="text" name="tablets_number" value="{{old('tablets_number')}}" class="form-control"/>
        </div>
        <div class="col-6 form-group floatlabel mb-4" id="">
            <div class="custom-control custom-checkbox">
                <input name="order_option" type="checkbox" class="custom-control-input" id="order_option" >
                <label class="custom-control-label" for="order_option">Order Option</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-12 form-group mb-4" id="features">
            <div class="form-group feature">
                <div class="form-group col-md-5 float-md-left">
                    <input type="text" type="text" name="features[0][name]" class="form-control" placeholder="Feature">
                </div>
                <div class="form-group col-md-5 float-md-left">
                    <input type="checkbox" name="features[0][active]" class="form-control col-md-1 float-md-left">
                    <label class="label col-md-11 float-md-left" for="form_header" style="margin-top: 7px;">Active</label>
                </div>
            </div>
        </div>
        <input onclick="addFeature()" type="button" class="btn btn-danger col-md-4 float-md-left" value="+ Add a Feature"/>


    </div>
    <div class="form-group text-center mt-5">
        <button type="submit" class="dark-btn">ADD PLAN</button>
    </div>
    {{Form::close()}}
@stop

@section('scripts')
    <style>
        #features {
            border: 1px solid #eaeaea;
            padding: 10px;
            border-radius: 10px;
        }
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="{{asset('assets/js/select2.full.min.js')}}"></script>
    <script>

        $.validate({modules: 'security'});

        $("#country_id").trigger("change");

        $('#country_id').change(function () {
            id = $(this).val()
            if (id) {
                $.ajax({
                    method: "GET",
                    url: "{{URL::to('admin/ajax/cities')}}",
                    data: {id: id}
                })
                    .done(function (data) {
                        $('#city_id').append(data)
                        console.log(data)
                    });
            }
        });

        $('#type').on('change',function(){
            let val = $('#type').val();
            if(val == 'Premium') {
                $('#price').val('Get A Quote');
                $('#price').attr('readonly', true);
                $('#tablets_number').val(0);
                $('#tablets_number_container').css('display','none');
            } else {
                $('#price').val('');
                $('#price').attr('readonly', false);
                $('#tablets_number').val('');
                $('#tablets_number_container').css('display','block');
            }
        });
        $(".select2").select2();

    </script>

    <script>
        var featureIndex = 0;

        function addFeature(){
            featureIndex ++;
            var html = `
                <div class="form-group feature">
                    <div class="form-group col-md-5 float-md-left">
                        <input type="text" type="text" name="features[`+featureIndex+`][name]" class="form-control" placeholder="Feature">
                    </div>
                    <div class="form-group col-md-5 float-md-left">
                        <input type="checkbox" name="features[`+featureIndex+`][active]" class="form-control col-md-1 float-md-left">
                        <label class="label col-md-11 float-md-left" for="form_header" style="margin-top: 7px;">Active</label>
                    </div>
                    <div class="form-group col-md-2 float-md-left">
                        <input onclick="remove(this)" type="button" class="btn btn-danger float-md-left" value="Delete"/>
                    </div>
                </div>`;

            $('#features').append(html);
        }

        function remove(e) {
            e.closest('div.feature').remove();
        }
    </script> 


@stop
