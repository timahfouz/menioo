<div class="side-nav-bar">
    <ul>
        @if(isset($navigations))
            @foreach($navigations as $navigation)
                <li><a href="{{$navigation->url}}"><i class="{{$navigation->class}}"></i>
                        <span>{{$navigation->name}}</span></a></li>
            @endforeach
        @endif
    </ul>
</div>