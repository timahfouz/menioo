<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title->name . (isset($title->parent) ? ' | '.$title->parent->name:'' ) }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('assets/css/all.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('assets/css/button-toggler.css')}}">
</head>