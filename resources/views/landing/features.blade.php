@extends('landing.layouts.app')

@section('title','Menioo | Features')

@section('header')
	@include('landing.common.header')
@endsection



@section('content')
	@include('landing.pages.features.content')
@endsection



@section('footer')
	@include('landing.common.footer')
@endsection