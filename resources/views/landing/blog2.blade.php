@extends('landing.layouts.app')


@section('header')
    @include('landing.common.header')
@endsection



@section('content')
    @include('landing.pages.blog.blog2')
@endsection



@section('footer')
    @include('landing.common.footer')
@endsection
