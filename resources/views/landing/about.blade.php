@extends('landing.layouts.app')


@section('header')
	@include('landing.common.header')
@endsection



@section('content')
	@include('landing.pages.about.content')
@endsection



@section('footer')
	@include('landing.common.footer')
@endsection