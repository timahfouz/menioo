@extends('landing.layouts.app')


@section('header')
    @include('landing.common.blank-header')
@endsection



@section('content')
    @include('landing.pages.quote.form')
@endsection



{{--@section('footer')
    @include('landing.common.footer')
@endsection--}}
