<section class="my-5 p-5">
    <div class="row">
        <div class="col-md-6 col-12">
            <h2 class="text-theme">Get started today and digitize your menu!</h2>
            <h4 class="my-2">Sign up for our starter plan and use up to three tablets for free!</h4>
        </div>
        <div class="col-md-6 col-12">
            <div class="input-group newsletter">
                <input type="email" class="form-control" placeholder="Enter your email">
                <span class="input-group-btn">
                    <button class="btn btn-primary">
                    @if(auth()->guard('diner')->check())
                            <a style="color: white;" href="{{ route('landing.pricing') }}">Sign Up</a>
                    @else
                            <a style="color: white;" href="{{ route('landing.sign-up') }}">Sign Up</a>
                     @endif
                    </button>
                </span>
            </div>
            <small class="text-muted">Use it for free. No credit card required.</small>
        </div>
    </div>
</section>