<!DOCTYPE html>
<html>
<head>
	<title>@yield('title','Menioo')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/ico" href="{{ asset('landing/images/favicon.ico') }}">
    <title>Menioo</title>
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">

	@stack('styles')

</head>
<body>

	@yield('header')

	@yield('content')
	
	@yield('footer')


    <script src="{{asset('landing/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('landing/js/popper.min.js')}}"></script>
    <script src="{{asset('landing/js/bootstrap.min.js')}}"></script>

    <script> new WOW().init(); </script>

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script>
    	function hideMessages() {
		    setTimeout(function () {
		            $(".alert").fadeOut();
		        },
		        2000
		    );
		}
    	hideMessages();
    </script>
    <!-- Calendly inline widget begin -->
    <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
    <!-- Calendly inline widget end -->



        <script>
            window.intercomSettings = {
                app_id: "j13srl2r",
            };
        </script>


    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/j13srl2r';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>


    @stack('scripts')
</body>
</html>
