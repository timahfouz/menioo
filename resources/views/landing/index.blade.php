@extends('landing.layouts.app')


@section('header')
	@include('landing.common.main-header')
    <style>
        .navbar.activate {
            background: linear-gradient(to right, #01BFD9,#CE00FF);
            -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
            -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
        }
    </style>
@endsection



@section('content')
	@include('landing.pages.index.content')
@endsection



@section('footer')
	@include('landing.common.footer')
@endsection