<!-- content -->
<section class="container my-5 p-5">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <h2 class="text-theme my-5 text-center">Free resources to help make you be a better manager and work more efficient.</h2>
            <p class="mt-3 text-center textGide">Guides designed to help you kick start and grow your restaurant! </p>
        </div>
    </div>

    <div class="row m-0 p-4 blogs align-items-center flex-row-reverse " >
        <div class="col-md-6 col-12">
            <h4 class="mb-4">Why did we develop menioo?</h4>
            <p class="ptext">
                Being in the restaurant business ourselves, we are very aware of the
                difficulty involved in developing a menu and how much of a pain
                it can be to change a price or add an item after the menu has
                already been printed. We were determined to figure out a way for
                restaurants to quickly and easily change prices, add items…
            </p>
            <a href="blog1" class="btn btn-link p-0 readon">Read More</a>
        </div>
        <div class="col-md-6 col-12 text-left">
            <img src="{{asset('landing/images/blog.png')}}" height="250px" width="100%">
        </div>
    </div><br>
    <div class="row mb-4">
        <div class="col-md-6 col-12 pt-2">
            <div class="blogs">
                <img src="{{asset('landing/images/blog-2.png')}}" class="img-fluid">
                <h4 class="my-2 pt-3">Solving the Ticket Time Riddle</h4>
                <p class="pt-4">
                    Restaurant owners and managers all know that
                    collecting feedback from guests can be a challenge,
                    especially honest real-time feedback…
                </p>
                <a href="blog2" class="btn btn-link p-0 readon">Read More</a>
            </div>
        </div>
        <div class="col-md-6 pt-2 col-12">
            <div class="blogs">
                <img src="{{asset('landing/images/blog-3.png')}}" class="img-fluid">
                <h4 class="my-2">Adding Calorie Count! An up-seller? Or a
                    down-seller?</h4>
                <p class="pt-3">
                    Let’s agree on one fact, guests who don’t care about
                    their calorie intake will not even read that information
                    on your menu…
                </p>
                <a href="blog3" class="btn btn-link p-0 readon">Read More</a>
            </div>
        </div>
    </div>



</section>

<section id="resources" class="mt-5 p-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <h2 class="text-theme my-3 pt-3 txtEdit mx-auto text-center">Download the app on iPad or Android tablet for
                    stunning dinning experience for your guests!</h2>
            </div>
        </div>
    </div>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')