<!-- Modal -->
<div class="modal fade" id="calendlyFire" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="calendly-inline-widget" data-url="https://calendly.com/mohamedelserag4488/30min" style="min-width:320px;height:630px;"></div>
            </div>
        </div>
    </div>
</div>