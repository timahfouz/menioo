<div class="wrapper-header pt-5">
    <div class="left wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
        <h2 class="text-light">Say Goodbye to printed menus
            and create your own digital tablet
            menu instantly! </h2>
        <p class="text-light pt-3">Bring your menu to life and watch your restaurant
            grow! Sign up for our starter plan and create menus on
            up to three tablets for free.</p>


        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <a class="btn btn-primary btnTry font-weight-bold textAbout d-flex justify-content-center shadow px-xl-4 px-4 py-2 mt-3 mb-1" href="{{ route('landing.login') }}">Try it for Free Forever</a>
                    <p class="text-center"><small class="text-white mb-3 font-italic">Up to 3 tablet menus</small></p>
                </div>
                <div class="col-lg-6 float-right">
                    <a class="btn btn-white-outline d-flex justify-content-center py-2 textAbout shadow-sm font-weight-bold px-4 px-xl-4 my-3" data-toggle="modal" data-target="#calendlyFire" href="">Schedule a Demo</a>
                </div>
            </div>
        </div>

        <ul class="d-flex mt-4 align-items-center mb-3">
            <li class="text-white mr-3">Available on:</li>
            <li class="mr-3"><a href="#"><img src="{{ asset('landing/images/apple-ico.png') }}"></a></li>
            <li><a href="#"><img src="{{ asset('landing/images/android-ico.png') }}"></a></li>
        </ul>
    </div>
    <div class="right text-right wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <img class="img-fluid" src="{{ asset('landing/images/header-img.png') }}">
    </div>
</div>


<section>
    <div class="section-wrapper mx-0 my-5 p-5 row">
        <div class="col-md-6 col-12 wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <img class="img-fluid" src="{{ asset('landing/images/why-img.png') }}">
        </div>
        <div class="col-md-6 col-12 wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.2s">
            <h2 class="text-theme mt-5">Why use a digital menu?</h2>
            <p class="py-3 digitalText">
                Digital restaurant menus can open up a whole new world
                of opportunities for your restaurant with striking visuals,
                a fun and interactive menu, and engaging descriptions
                that are easy to read, so that your diners can fully
                immerse themselves in a world of food, flavors, and scents!
            </p>
        </div>
    </div>
</section>



<section id="features" class="my-5 p-5">
    <h2 class="text-theme my-5 text-center"> Features </h2>
    <div class="inner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-12 d-flex my-4 align-items-center">
                    <div class="row">
                        <div class="col-lg-3"><img class="d-flex mx-auto pb-3" src="{{ asset('landing/images/features-ico-1.png') }}">
                        </div>

                        <div class="col-lg-9">
                            <h3>Menu Management</h3>
                            <p>With an easy to use web interface, create as many menus as you need, change or add items
                                at any point of time and publish instantly to your tablets.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 d-flex my-3 align-items-center">

                    <div class="row">
                        <div class="col-lg-3"><img class="d-flex mx-auto pb-3" src="{{ asset('landing/images/features-ico-.png') }}">
                        </div>

                        <div class="col-lg-9">
                            <h4>Design Customization</h4>
                            <p>Choose your own theme, color, and design. Create a unique looking menu that emphasizes
                                the essence of your restaurant and the dishes you serve.
                            </p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-12 d-flex my-3 align-items-center">
                    <div class="row">
                        <div class="col-lg-3"><img class="d-flex mx-auto pb-3" src="{{ asset('landing/images/features-ico-3.png') }}">
                        </div>

                        <div class="col-lg-9">
                            <h4>Order Traking</h4>
                            <p>Your guests can make instant orders on the tablet that are sent directly to the
                                dashboard, to your POS or to your kitchen printers right away. </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12 d-flex my-3 align-items-center">
                    <div class="row">
                        <div class="col-lg-3"><img class="d-flex mx-auto pb-3" src="{{ asset('landing/images/features-ico-4.png') }}">
                        </div>

                        <div class="col-lg-9">
                            <h4>Feedback Collection</h4>
                            <p>Collect instant feedback from your guests and prevent your staff from tampering with the
                                information since they are not printed anymore. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="mt-5 py-5 text-center">
            <a href="{{route('landing.about')}}" class="btn btn-primary textAbout px-5">Discover More</a>
        </div>

    </div>
</section>


<section class="my-3 pt-3 wow zoomIn" data-wow-duration="1.6s" data-wow-delay="0.8s">
    <h1 class="text-theme my-3 text-center">How does it work?</h1>
    <p class="text-center txtVideo mb-5 pt-3">Three simple steps to get your menu ready!</p>
    <div class="video text-center">
        <button class="btn btn-link"><img src="{{ asset('landing/images/video-play-ico.png') }}"></button>
        <img class="img-fluid" src="{{ asset('landing/images/video.png') }}">
    </div>
    <div class="py-5 text-center">
        <a href="{{route('landing.login')}}" class="btn py-2 btn-primary textAbout px-5"> Try it for Free </a>
    </div>
</section>

<section id="resources" class="my-5 p-5 resourses">
    <h2 class="text-theme pt-3 mb-5 text-center">Resources Made For You</h2>
    <div class="row">
        <div class="col-md-4 col-12 wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/resources-img-1.png') }}" class="img-fluid">
            <h4 class="my-2">Why did we develop <br> menioo?</h4>
            <p class="mt-3">
                Being in the restaurant business
                ourselves, we are very aware of the
                difficulty involved in developing…
            </p>
            <button class="btn btn-link p-0 readon">Read More</button>
        </div>
        <div class="col-md-4 col-12 wow zoomIn" data-wow-duration="1.6s" data-wow-delay="0.8s">
            <img src="{{ asset('landing/images/resources-img-2.png') }}" class="img-fluid">
            <h4 class="my-2">Solving the Ticket Time <br> Riddle</h4>
            <p class="mt-3">
                Restaurant owners and managers
                all know that collecting feedback
                from guests can be a challenge…
            </p>
            <button class="btn btn-link p-0 readon">Read More</button>
        </div>
        <div class="col-md-4 col-12 wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/resources-img-3.png') }}" class="img-fluid">
            <h4 class="my-2">Adding Calorie Count! An up-seller? Or a down-seller?</h4>
            <p class="mt-3">
                Let’s agree on one fact, guests who
                don’t care about their calorie intake
                will not even read that information…
            </p>
            <button class="btn btn-link p-0 readon">Read More</button>
        </div>
    </div>
    <div class="py-5 text-center">
        <a href="{{route('landing.login')}}" class="btn btn-primary textAbout px-5">Discover More</a>
    </div>
</section>

@include('landing.common.signupsection')

<section id="resources" class="mt-5 p-5 wow zoomIn" data-wow-duration="1.2s" data-wow-delay="0.4s">
    <h2 class="text-theme my-3 pt-3 txtEdit mx-auto text-center">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{ asset('landing/images/ios-btn.png') }}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{ asset('landing/images/android-btn.png') }}"></a></li>
    </ul>

</section>


@include('landing.pages.index.calendly')


<script src="{{asset('landing/js/jquery-3.4.1.min.js')}}"></script>
<script>
    $(window).on("scroll", function() {
        if($(window).scrollTop() > 70) {
            $(".navbar").addClass("activate");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $(".navbar").removeClass("activate");
        }
    });
</script>