<div class="login header-height mb-5">
    <div class="row m-0">
        <div class="col-md-1"></div>
        <div class="col-md-5 col-12 pb-4">

            @include('landing.layouts.error')
            {{Form::open(array('url'=>array('sign-up') ,'method'=>'POST','class'=>"form-signin"))}}

            <div class="form p-5">
                <h3 class="text-theme text-center mb-4">Sign Up</h3>
                <button class="btn btn-outline-primary rounded-0 btn-block">Sign up with Facebook</button>
                <div class="separator my-5 text-center"><span>or</span></div>
                <div class="form-group">
                    <label>Full Name<span class="required">*</span></label>
                    <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="James Dean">
                </div>
                <div class="form-group">
                    <label>Email Address<span class="required">*</span></label>
                    <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="James.dean@email.com">
                </div>
                <div class="form-group">
                    <label>Password<span class="required">*</span></label>
                    <input type="password" name="password" class="form-control" placeholder="Must be at least 8 characters">
                </div>
                <div class="form-group">
                    <label>Restaurant Name<span class="required">*</span></label>
                    <input type="text" name="company_name" value="{{old('company_name')}}" class="form-control" placeholder="Restaurant Name">
                </div>

                <button type="submit" class="btn btn-block btn-primary rounded-0">Start your free plan</button>

                <p class="my-2"><small>
                        By continuing you agree to Menioo’s
                        <a class="btn-link text-theme" href="{{ route('landing.terms') }}">Terms and Conditions</a> and
                        <a class="btn-link text-theme" href="{{ route('landing.privacy') }}">Privacy Policy</a></small></p>

                <p class="d-flex align-items-center justify-content-center">
                    <strong>Already have an account?</strong>
                    <a href="{{ route('landing.login') }}" class="text-theme btn btn-link">Log in</a>
                </p>
            </div>

            {{ Form::close() }}


        </div>
        <div class="col-md-6 col-12 text-right">
            <h3 class="text-theme text-center">Sign up for free and create <br>
                your first menu!</h3>
            <p class="text-theme text-center mt-5">
                Try Menioo up to 3 tablets for free. No credit<br>
                card needed and no surprises. Pinky promise.
            </p>
            <img src="{{asset('landing/images/header-img.png')}}" class="img-fluid mt-5">
        </div>
    </div>
</div>