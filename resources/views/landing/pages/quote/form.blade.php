<div class="login header-height mb-5">
    <div class="row m-0 pt-1">
        <div class="col-md-1"></div>
        <div class="col-md-10 col-12 wow zoomIn getQoteForm" data-wow-duration="2s" data-wow-delay="0.4s">
            <h2 class="text-theme text-center mb-3">Get a Quote</h2>
            <p class="text-theme textGetQote text-center mb-5">More details help us to give you an accurate quote</p>
            <div class="form p-5 mt-3">
                {{ Form::open(['route' => 'landing.send.quote', 'method' => 'post']) }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Full Name </label>
                                <input name="full_name" type="Text" required class="form-control" placeholder="James Dean">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" required class="form-control" placeholder="James.dean@email.com">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Region </label>
                                <select name="country" class="form-control" required>
                                    <option value="">Choose your country</option>
                                    @foreach($countries as $country)
                                    <option value="{{ $country->name }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input name="phone" type="tel" required class="form-control" placeholder="Add your number">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>How many tablets do you need? </label>
                                <input name="tablets" type="number" required class="form-control" placeholder="Add tablets number">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Restaurant Name</label>
                                <input name="restaurant_name" type="text" required class="form-control" placeholder="Add your restaurant name">
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3 mt-3">
                        <div class="col-lg-12 textArea">
                            <label>Note</label>
                            <textarea name="notes" class="form-control" placeholder="Add any additional notes" required rows="5" cols="25"></textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn d-flex mx-auto px-5 btnSend rounded-0"> Send </button>


                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
