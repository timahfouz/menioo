<div class="login header-height mb-5">
    <div class="row m-0">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <h4 class="text-theme text-center mb-4">Forgot your password?</h4>
            <p class="text-theme text-center">Enter your email address and we’ll send you <br> a link to reset your
                password.</p>
        </div>
    </div>
    <div class="row m-0">
        <div class="col-md-1"></div>
        <div class="col-md-5 col-12 mt-5">
            {{ Form::open(['url' => route('landing.reset.password'), 'method' => 'post']) }}
            <div class="form p-5">
                <h4 class="text-theme text-center mb-4">Reset Password</h4>
                <input type="hidden" name="token" value="{{$token}}">
                <div class="form-group">
                    <label>New Password <span class="required">*</span></label>
                    <input name="password" type="password" class="form-control" placeholder="Must be at least 8 characters">
                </div>
                <div class="form-group">
                    <label>Confirm New Password <span class="required">*</span></label>
                    <input name="password_confirmation" type="password" class="form-control" placeholder="Must be at least 8 characters">
                </div>
                <button class="btn btn-block btn-primary rounded-0">Reset Password</button>

            </div>
            {{ Form::close() }}
        </div>
        <div class="col-md-6 col-12 text-right">
            <img src="{{asset('landing/images/header-img.png')}}" class="img-fluid mt-5">
        </div>
    </div>
</div>