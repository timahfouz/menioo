
<!-- content -->
<section class="container my-5 p-5">
    <h2 class="text-theme my-5 text-center">About</h2>

    <div class="text-center">
        <img src="{{asset('landing/images/about.png')}}" class="img-fluid">
    </div>

    <p class="textAbout mt-3">
        Menioo.com is developed and owned by KingsTeck OÜ. KingsTeck OÜ is a technology company that is
        obsessed by developing solutions for the world. 
    </p>
    <p class="textAbout mt-3">Our company is established in the European Union in Estonia, located at Sepapaja 6, Tallinn 15551. </p>
    <p class="textAbout mb-5">
        We work with partners around the globe to develop solutions that help businesses generate higher
        revenues with efficiency. We have technology partners in the <strong>United States, Egypt, India and
            China</strong>
        that help us develop these solutions. 
    </p>

    <div class="text-center">
        <button class="btn btn-primary textAbout text-uppercase px-5">kingsteck Site</button>
    </div>

</section>
