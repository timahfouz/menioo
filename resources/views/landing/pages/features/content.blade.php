<!-- content -->
<section class="my-5 p-5 container">
    <h2 class="text-theme my-5 text-center">Features</h2>

    <div class="row my-5 align-items-center pt-3">
        <div class="col-md-6 col-12 wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <h4 class="mb-5">Create a customized menu</h4>
            <p class="ptext">With an easy to use web interface, you will be surprised by how fast
                you can create your very own custom digital tablet menu. </p>
            <p class="ptext">
                Create themes that vividly tell the story of your place. To encourage
                guests to return, you can develop holiday themes, seasonal themes
                and themes for any special occasion or celebration.
            </p>
        </div>
        <div class="col-md-6 col-12 text-right wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/features/01.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center flex-row-reverse wow zoomIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <div class="col-md-6 col-12">
            <h4 class="mb-5">Show detailed descriptions of each dish on
                your menu!</h4>
            <p class="ptext">
                A well-designed digital tablet menu will provide detailed descriptions
                of each dish. To further engage your guests, you might want to add
                a personal story on how certain dishes were created and by whom.
            </p>
        </div>
        <div class="col-md-6 col-12 text-left">
            <img src="{{ asset('landing/images/features/02.png')}}" alt="" class="img-fluid">
        </div>
    </div>


    <div class="row my-5 align-items-center">
        <div class="col-md-6 col-12 wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <h4 class="mb-5">Your guests can place orders right from their table</h4>
            <p class="ptext">
                Our digital tablet menu system integrates with your restaurant’s POS
                and orders cen be sent directly to your kitchen printers. Can you
                imagine a better way to streamline the process? Your orders appear
                on the control panel, making them easy to follow and manage.
            </p>

        </div>
        <div class="col-md-6 col-12 text-right wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/features/03.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center flex-row-reverse wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <div class="col-md-6 col-12">
            <h4 class="mb-5">Show each dish’s prep time</h4>
            <p class="ptext">
                Enhance the guest experience by displaying the time it takes to
                prepare each dish on your digital menu. Customers in a hurry
                can make an informed decision when placing their order and
                you don’t risk annoying customers unnecessarily with a longer
                wait than expected.
            </p>
        </div>
        <div class="col-md-6 col-12 text-left">
            <img src="{{ asset('landing/images/features/04.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center wow zoomIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <div class="col-md-6 col-12">
            <h4 class="mb-5">Flexibility in making price changes</h4>
            <p class="ptext">
                To improve sales, you have the flexibility to vary your prices to
                keep customers happy. For example, you can price menu items
                according to plate size, portion size, and choice of toppings.
            </p>

        </div>
        <div class="col-md-6 col-12 text-right">
            <img src="{{ asset('landing/images/features/05.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center flex-row-reverse">
        <div class="col-md-6 col-12 wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.4s">
            <h4 class="mb-5">Get customer feedback in real time</h4>
            <p class="ptext">
                Your guests can use the digital menu to offer immediate feedback to
                management. This allows you to quickly determine customer needs,
                what you’re doing well and what may need improvement. This will
                prevent your staff from tampering with the guest feedbacks.
            </p>
            <p class="ptext">Get instant access to your guests’ feedback from anywhere in the
                world through your online dashboard.</p>
        </div>
        <div class="col-md-6 col-12 text-left wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/features/06.png')}}" alt="" class="img-fluid">
        </div>
    </div>


    <div class="row my-5 align-items-center wow zoomIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <div class="col-md-6 col-12">
            <h4 class="mb-5">Calorie-count can be displayed</h4>
            <p class="ptext">Many restaurant guests watch their calories. Help your customers by
                displaying the calories of each dish beside its description on your
                digital menu.
            </p>

        </div>
        <div class="col-md-6 col-12 text-right">
            <img src="{{ asset('landing/images/features/07.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center flex-row-reverse">
        <div class="col-md-6 col-12 wow bounceInRight" data-wow-duration="2s" data-wow-delay="0.4s">
            <h4 class="mb-5">Caution your guests</h4>
            <p class="ptext">Embellish your digital menu with important information like specific
                ingredients that people tend to be allergic to. You can also provide
                alternatives and substitutions on your menu to improve the
                guest experience. </p>

        </div>
        <div class="col-md-6 col-12 text-left wow bounceInLeft" data-wow-duration="2s" data-wow-delay="0.4s">
            <img src="{{ asset('landing/images/features/08.png')}}" alt="" class="img-fluid">
        </div>
    </div>

    <div class="row my-5 align-items-center wow zoomIn" data-wow-duration="2s" data-wow-delay="0.4s">
        <div class="col-md-6 col-12">
            <h4 class="mb-5">Take advantage of data analytics</h4>
            <p class="ptext">The one thing all guests have in common is that they all peruse
                your menu. Since this is a digital menu, it gathers insights. menioo
                was developed so it could follow each guest’s actions as they look
                over the menu. </p>
            <p class="ptext">Your dashboard will show you which menu items were viewed the
                most and which ones the least. You can see your daily sales, including
                average ticket size, the revenue share among categories and more!
                You now have a record of what your guests do when using your menu,
                which allows you to take steps to improve sales. </p>
            <p class="ptext">And all this wealth of information, is stored in the cloud with access from
                anywhere in the world. </p>

        </div>
        <div class="col-md-6 col-12 text-right">
            <img src="{{ asset('landing/images/features/09.png')}}" alt="" class="img-fluid">
        </div>
    </div>

</section>


<section id="resources" class="mt-5 p-5 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
    <h2 class="text-theme my-3 mx-auto text-center w-75">Download the app on iPad or Android tablet for
        stunning dinning experience for your guests!</h2>

    <ul class="d-flex flex-wrap justify-content-center my-5">
        <li class="mx-3 my-2"><a href="#"><img src="{{ asset('landing/images/ios-btn.png')}}"></a></li>
        <li class="mx-3 my-2"><a href="#"><img src="{{ asset('landing/images/android-btn.png')}}"></a></li>
    </ul>

</section>


@include('landing.common.signupsection')
