<div class="login header-height mb-5">
    <div class="row m-0">
        <div class="col-md-1"></div>
        <div class="col-md-5 col-12 pb-4">

            @include('diner.layouts.error')
            {{Form::open(array('url'=>array('login') ,'method'=>'POST','class'=>"form-signin"))}}

            <div class="form p-5">
                <h2 class="text-theme text-center mb-4">Login</h2>

                <a href="{{ route('landing.redirect.facebook',['provider' => 'facebook']) }}" class="btn btn-outline-primary rounded-0 btn-block">Sign up with Facebook</a>
                <div class="separator my-5 text-center"><span>or</span></div>
                <div class="form-group">
                    <label>Email Address<span class="required">*</span></label>
                    <input name="email" type="email" class="form-control" placeholder="James.dean@email.com">
                </div>
                <div class="form-group">
                    <label>Password<span class="required">*</span></label>
                    <input name="password" type="password" class="form-control" placeholder="Must be at least 8 characters">
                </div>
                <div class="d-flex align-items-center">
                    <div class="custom-control d-inline custom-checkbox">
                        <input name="remember" value="1" type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label" for="customCheck1">Remember me</label>
                    </div>
                    <a href="{{ route('landing.forget.password.view') }}" class="btn btn-link ml-auto text-theme">Forgot your password?</a>
                </div>
                <button type="submit" class="btn btn-block btn-primary rounded-0">Log in</button>
                <p class="d-flex align-items-center justify-content-center">
                    <strong>Don't have an account?</strong>
                    <a href="{{ route('landing.sign-up') }}" class="text-theme btn btn-link">Sign up</a>
                </p>
            </div>
            {{ Form::close() }}

        </div>
        <div class="col-md-6 col-12 text-right pt-2">
            <h4 class="text-theme text-center mb-4">It’s good to see you again</h4>
            <p class="text-theme text-center">Welcome back to Menioo. Sign in to get going</p>
            <img src="{{ asset('landing/images/header-img.png') }}" class="img-fluid mt-5">
        </div>
    </div>
</div>