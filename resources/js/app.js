require('./bootstrap');


import Vue from 'vue'
import VueRouter from 'vue-router';
import Home from './components/Home.vue';

import { routes } from './routes';
Vue.use(VueRouter);
const routers = new VueRouter({
    mode: 'history',
    base: 'menioo/public/diner/preview',
    routes
});

new Vue({
    el: '#app',
    // devServer: {
    //     host: '0.0.0.0',
    //     port: '8080',
    //     public: 'mydomain.example.com:8080',
    // },
    components: {
        Home,
    },
    router: routers,
});
