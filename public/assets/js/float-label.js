function initFloatLabels() {
    let inputs = $(".floatlabel input");
    let textareas = $(".floatlabel textarea");
    let selects = $(".floatlabel select");
    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].value != "") {
            $('#' + inputs[i].id).siblings('label').addClass('floatlabel-shift');
        }
    };

    for (let i = 0; i < textareas.length; i++) {
        if (textareas[i].value != "") {
            $('#' + textareas[i].id).siblings('label').addClass('floatlabel-shift');
        }
    };

    for (let i = 0; i < selects.length; i++) {
        if (selects[i].value != "") {
            $('#' + selects[i].id).siblings('label').addClass('floatlabel-shift');
        }
    };

    ////inputs
    $(".floatlabel input").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-shift');
    });

    $(".floatlabel input").blur(function () {
        if ($(this).val().length == 0) {
            let elementId = $(this).attr('id');
            $("label[for=" + elementId + "]").removeClass('floatlabel-shift');
        }
    });

    $(".floatlabel input").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-active');
    });

    $(".floatlabel input").blur(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").removeClass('floatlabel-active');
    });


    //// textareas
    $(".floatlabel textarea").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-shift');
    });

    $(".floatlabel textarea").blur(function () {
        if ($(this).val().length == 0) {
            let elementId = $(this).attr('id');
            $("label[for=" + elementId + "]").removeClass('floatlabel-shift');
        }
    });

    $(".floatlabel textarea").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-active');
    });

    $(".floatlabel textarea").blur(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").removeClass('floatlabel-active');
    });

    //// select
    $(".floatlabel select").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-shift');
    });

    $(".floatlabel select").blur(function () {
        if ($(this).val().length == 0) {
            let elementId = $(this).attr('id');
            $("label[for=" + elementId + "]").removeClass('floatlabel-shift');
        }
    });

    $(".floatlabel select").focus(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").addClass('floatlabel-active');
    });

    $(".floatlabel select").blur(function () {
        let elementId = $(this).attr('id');
        $("label[for=" + elementId + "]").removeClass('floatlabel-active');
    });
}
initFloatLabels();